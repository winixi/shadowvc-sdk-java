package com.shadowvc.sdk;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.internal.util.ApiResponse;

/**
 * ECBOX客户端。
 * <p>
 * File: ApiClient.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public interface ApiClient {

  /**
   * 执行TOP公开API请求。
   *
   * @param <T> 泛型
   * @param request 具体的TOP请求
   * @return 返回
   * @throws ApiException 异常
   */
  <T extends ApiResponse> T execute(ApiRequest<T> request) throws ApiException;

  /**
   * 执行TOP隐私API请求。
   *
   * @param <T> 泛型
   * @param request     具体的TOP请求
   * @param accessToken 用户会话授权码
   * @return 返回
   * @throws ApiException 异常
   */
  <T extends ApiResponse> T execute(ApiRequest<T> request, String accessToken) throws ApiException;
}
