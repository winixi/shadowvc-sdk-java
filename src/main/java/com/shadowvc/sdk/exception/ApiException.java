package com.shadowvc.sdk.exception;

/**
 * 客户端异常
 * 
 * File: ApiException.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 * 
 * @author chenxiaochun
 * @version 1.0
 */
public class ApiException extends Exception {

  private static final long serialVersionUID = -238091758285157331L;

  private Integer errCode;
  private String errMsg;

  public ApiException() {
    super();
  }

  public ApiException(String message, Throwable cause) {
    super(message, cause);
  }

  public ApiException(String message) {
    super(message);
  }

  public ApiException(Throwable cause) {
    super(cause);
  }

  public ApiException(Integer errCode, String errMsg) {
    super(errCode + ":" + errMsg);
    this.errCode = errCode;
    this.errMsg = errMsg;
  }

  public Integer getErrCode() {
    return this.errCode;
  }

  public String getErrMsg() {
    return this.errMsg;
  }

  public String getMessage() {
    if (errCode != null && errMsg != null) {
      return errCode + ":" + errMsg;
    }
    return super.getMessage();
  }

}
