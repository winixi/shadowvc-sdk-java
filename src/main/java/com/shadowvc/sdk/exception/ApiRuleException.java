package com.shadowvc.sdk.exception;

/**
 * API前置检查异常。
 * 
 * File: ApiRuleException.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ApiRuleException extends ApiException {

  private static final long serialVersionUID = -7787145910600194272L;

  public ApiRuleException(Integer errCode, String errMsg) {
    super(errCode, errMsg);
  }

}
