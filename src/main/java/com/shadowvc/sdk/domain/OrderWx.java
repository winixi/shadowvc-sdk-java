package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiObject;

public class OrderWx extends ApiObject {

  @ApiField
  private String appid;
 

  @ApiField
  private String no;
 

  @ApiField
  private String orderType;
 

  @ApiField
  private Integer price;
 

  @ApiField
  private String payType;
 

  @ApiField
  private String status;
 

  @ApiField
  private String payStatus;
 

  @ApiField
  private String payTime;
 

  @ApiField
  private String refundStatus;
 

  @ApiField
  private String refundTime;
 

  @ApiField
  private String orderDesc;
 

  @ApiField
  private String clientIp;
 

  @ApiField
  private String tradeType;
 

  @ApiField
  private String openId;
 

  @ApiField
  private String created;


  public String getAppid() {
    return appid;
  }

  public void setAppid(String appid) {
    this.appid = appid;
  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public String getPayType() {
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPayStatus() {
    return payStatus;
  }

  public void setPayStatus(String payStatus) {
    this.payStatus = payStatus;
  }

  public String getPayTime() {
    return payTime;
  }

  public void setPayTime(String payTime) {
    this.payTime = payTime;
  }

  public String getRefundStatus() {
    return refundStatus;
  }

  public void setRefundStatus(String refundStatus) {
    this.refundStatus = refundStatus;
  }

  public String getRefundTime() {
    return refundTime;
  }

  public void setRefundTime(String refundTime) {
    this.refundTime = refundTime;
  }

  public String getOrderDesc() {
    return orderDesc;
  }

  public void setOrderDesc(String orderDesc) {
    this.orderDesc = orderDesc;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public String getTradeType() {
    return tradeType;
  }

  public void setTradeType(String tradeType) {
    this.tradeType = tradeType;
  }

  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  @Override
  public String toString() {
    return "OrderWx{" +
            "appid='" + appid + '\'' +
            ", no='" + no + '\'' +
            ", orderType='" + orderType + '\'' +
            ", price=" + price +
            ", payType='" + payType + '\'' +
            ", status='" + status + '\'' +
            ", payStatus='" + payStatus + '\'' +
            ", payTime='" + payTime + '\'' +
            ", refundStatus='" + refundStatus + '\'' +
            ", refundTime='" + refundTime + '\'' +
            ", orderDesc='" + orderDesc + '\'' +
            ", clientIp='" + clientIp + '\'' +
            ", tradeType='" + tradeType + '\'' +
            ", openId='" + openId + '\'' +
            ", created='" + created + '\'' +
            '}';
  }
}