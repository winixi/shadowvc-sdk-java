package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.Date;


/**
 * 领域模型
 * 
 * File: Agreement.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Agreement extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String uuid;

  @ApiField
  private String content;

  @ApiField
  private Date created;

  @ApiField
  private Date updated;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  @Override
  public String toString() {
    return "Agreement{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", content='" + content + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            '}';
  }
}