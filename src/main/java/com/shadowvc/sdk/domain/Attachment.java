package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Attachment.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Attachment extends ApiObject {

  @ApiField
  private Integer companyId;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String name;

  @ApiField
  private String format;

  @ApiField
  private Long size;

  @ApiField
  private String storageKey;

  @ApiField
  private String outId;

  @ApiField
  private String url;

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFormat() {
    return this.format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public Long getSize() {
    return this.size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public String getStorageKey() {
    return this.storageKey;
  }

  public void setStorageKey(String storageKey) {
    this.storageKey = storageKey;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return "Attachment{" +
            "companyId=" + companyId +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", name='" + name + '\'' +
            ", format='" + format + '\'' +
            ", size=" + size +
            ", storageKey='" + storageKey + '\'' +
            ", outId='" + outId + '\'' +
            ", url='" + url + '\'' +
            '}';
  }
}