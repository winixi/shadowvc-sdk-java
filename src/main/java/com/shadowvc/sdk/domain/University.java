package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: University.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class University extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String name;

  @ApiField
  private String department;

  @ApiField
  private String area;

  @ApiField
  private String schoolLevel;

  @ApiField
  private String schoolType;

  @ApiField
  private String remark;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDepartment() {
    return this.department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getArea() {
    return this.area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getSchoolLevel() {
    return this.schoolLevel;
  }

  public void setSchoolLevel(String schoolLevel) {
    this.schoolLevel = schoolLevel;
  }

  public String getSchoolType() {
    return this.schoolType;
  }

  public void setSchoolType(String schoolType) {
    this.schoolType = schoolType;
  }

  public String getRemark() {
    return this.remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @Override
  public String toString() {
    return "University{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", department='" + department + '\'' +
            ", area='" + area + '\'' +
            ", schoolLevel='" + schoolLevel + '\'' +
            ", schoolType='" + schoolType + '\'' +
            ", remark='" + remark + '\'' +
            '}';
  }
}