package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Country.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Country extends ApiObject {

  @ApiField
  private String name;

  @ApiField
  private String zhName;

  @ApiField
  private String code;

  @ApiField
  private String code2;

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getZhName() {
    return this.zhName;
  }

  public void setZhName(String zhName) {
    this.zhName = zhName;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode2() {
    return this.code2;
  }

  public void setCode2(String code2) {
    this.code2 = code2;
  }

  @Override
  public String toString() {
    return "Country{" +
            "name='" + name + '\'' +
            ", zhName='" + zhName + '\'' +
            ", code='" + code + '\'' +
            ", code2='" + code2 + '\'' +
            '}';
  }
}