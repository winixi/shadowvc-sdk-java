package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Alist.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Alist extends ApiObject {

  @ApiField
  private Integer total;

  @ApiListField
  private List<?> list;

  public Integer getTotal() {
    return this.total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<?> getList() {
    return this.list;
  }

  public void setList(List<?> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "Alist{" +
            "total=" + total +
            ", list=" + list +
            '}';
  }
}