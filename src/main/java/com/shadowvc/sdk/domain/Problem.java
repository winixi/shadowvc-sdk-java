package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.Date;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Problem.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Problem extends ApiObject {

  @ApiField
  private String uuid;

  @ApiField
  private String title;

  @ApiField
  private String label;

  @ApiField
  private Integer categoryId;

  @ApiField
  private String content;

  @ApiField
  private String wxOpenId;

  @ApiField
  private Date updated;

  @ApiListField
  private List<String> tags;

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return "Problem{" +
            "uuid='" + uuid + '\'' +
            ", title='" + title + '\'' +
            ", label='" + label + '\'' +
            ", categoryId=" + categoryId +
            ", content='" + content + '\'' +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", updated=" + updated +
            ", tags=" + tags +
            '}';
  }
}