package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Express.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Express extends ApiObject {

  @ApiField
  private String abbr;

  @ApiField
  private String name;

  public String getAbbr() {
    return this.abbr;
  }

  public void setAbbr(String abbr) {
    this.abbr = abbr;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Express{" +
            "abbr='" + abbr + '\'' +
            ", name='" + name + '\'' +
            '}';
  }
}