package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Version.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Version extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private Double ver;

  @ApiField
  private AppType appType;

  @ApiField
  private String url;

  @ApiField
  private Boolean forceUpdate;

  @ApiField
  private String memo;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Double getVer() {
    return this.ver;
  }

  public void setVer(Double ver) {
    this.ver = ver;
  }

  public AppType getAppType() {
    return this.appType;
  }

  public void setAppType(AppType appType) {
    this.appType = appType;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getForceUpdate() {
    return this.forceUpdate;
  }

  public void setForceUpdate(Boolean forceUpdate) {
    this.forceUpdate = forceUpdate;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  @Override
  public String toString() {
    return "Version{" +
            "id=" + id +
            ", ver=" + ver +
            ", appType=" + appType +
            ", url='" + url + '\'' +
            ", forceUpdate=" + forceUpdate +
            ", memo='" + memo + '\'' +
            '}';
  }
}