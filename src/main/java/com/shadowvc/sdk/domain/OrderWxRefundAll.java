package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.annotation.ApiField;

/**
 * Created by motorfu on 16/9/19.
 */
public class OrderWxRefundAll {
  @ApiField
  private Long price;

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "OrderWxRefundAll{" +
            "price=" + price +
            '}';
  }
}
