package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Tag.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Tag extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String name;

  @ApiField
  private Integer hot;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getHot() {
    return this.hot;
  }

  public void setHot(Integer hot) {
    this.hot = hot;
  }

  @Override
  public String toString() {
    return "Tag{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", hot=" + hot +
            '}';
  }
}