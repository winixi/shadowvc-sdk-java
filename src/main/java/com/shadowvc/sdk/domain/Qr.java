package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Qr.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Qr extends ApiObject {

  @ApiField
  private String data;

  @ApiField
  private Integer size;

  @ApiField
  private Integer margin;

  public String getData() {
    return this.data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Integer getSize() {
    return this.size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getMargin() {
    return this.margin;
  }

  public void setMargin(Integer margin) {
    this.margin = margin;
  }

  @Override
  public String toString() {
    return "Qr{" +
            "data='" + data + '\'' +
            ", size=" + size +
            ", margin=" + margin +
            '}';
  }
}