package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.CommentStatus;
import com.shadowvc.sdk.dict.ReplyStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.Date;


/**
 * 领域模型
 * 
 * File: Comment.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Comment extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String outId;

  @ApiField
  private String label;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String content;

  @ApiField
  private String reply;

  @ApiField
  private String email;

  @ApiField
  private String mobile;

  @ApiField
  private CommentStatus commentStatus;

  @ApiField
  private ReplyStatus replyStatus;

  @ApiField
  private Integer praise;

  @ApiField
  private Date created;

  @ApiField
  private Date updated;

  @ApiField
  private Date replyTime;

  @ApiField
  private Boolean hadPraise;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getReply() {
    return this.reply;
  }

  public void setReply(String reply) {
    this.reply = reply;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public CommentStatus getCommentStatus() {
    return this.commentStatus;
  }

  public void setCommentStatus(CommentStatus commentStatus) {
    this.commentStatus = commentStatus;
  }

  public ReplyStatus getReplyStatus() {
    return this.replyStatus;
  }

  public void setReplyStatus(ReplyStatus replyStatus) {
    this.replyStatus = replyStatus;
  }

  public Integer getPraise() {
    return this.praise;
  }

  public void setPraise(Integer praise) {
    this.praise = praise;
  }

  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public Date getReplyTime() {
    return this.replyTime;
  }

  public void setReplyTime(Date replyTime) {
    this.replyTime = replyTime;
  }

  public Boolean getHadPraise() {
    return this.hadPraise;
  }

  public void setHadPraise(Boolean hadPraise) {
    this.hadPraise = hadPraise;
  }

  @Override
  public String toString() {
    return "Comment{" +
            "id=" + id +
            ", outId='" + outId + '\'' +
            ", label='" + label + '\'' +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", content='" + content + '\'' +
            ", reply='" + reply + '\'' +
            ", email='" + email + '\'' +
            ", mobile='" + mobile + '\'' +
            ", commentStatus=" + commentStatus +
            ", replyStatus=" + replyStatus +
            ", praise=" + praise +
            ", created=" + created +
            ", updated=" + updated +
            ", replyTime=" + replyTime +
            ", hadPraise=" + hadPraise +
            '}';
  }
}