package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.ProductStatus;
import com.shadowvc.sdk.dict.ProductType;
import com.shadowvc.sdk.domain.Category;
import com.shadowvc.sdk.domain.Company;
import com.shadowvc.sdk.domain.Copyright;
import com.shadowvc.sdk.domain.Tag;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.Date;
import java.util.List;


/**
 * 领域模型
 * 
 * File: ProductSoftware.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductSoftware extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String uuid;

  @ApiField
  private ProductStatus productStatus;

  @ApiField
  private ProductType productType;

  @ApiField
  private String name;

  @ApiField
  private String detail;

  @ApiField
  private String brand;

  @ApiField
  private String logo;

  @ApiField
  private Category category;

  @ApiField
  private Integer categoryId;

  @ApiField
  private Company company;

  @ApiField
  private Integer companyId;

  @ApiField
  private Date updated;

  @ApiListField
  private List<Copyright> copyrights;

  @ApiListField
  private List<Tag> tags;

  @ApiField
  private String url;

  @ApiField
  private String serviceTel;

  @ApiField
  private String wxSubscribe;

  @ApiField
  private String wxService;

  @ApiField
  private String weibo;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public ProductStatus getProductStatus() {
    return this.productStatus;
  }

  public void setProductStatus(ProductStatus productStatus) {
    this.productStatus = productStatus;
  }

  public ProductType getProductType() {
    return this.productType;
  }

  public void setProductType(ProductType productType) {
    this.productType = productType;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDetail() {
    return this.detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getBrand() {
    return this.brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public Category getCategory() {
    return this.category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public Company getCompany() {
    return this.company;
  }

  public void setCompany(Company company) {
    this.company = company;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public List<Copyright> getCopyrights() {
    return this.copyrights;
  }

  public void setCopyrights(List<Copyright> copyrights) {
    this.copyrights = copyrights;
  }

  public List<Tag> getTags() {
    return this.tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getServiceTel() {
    return this.serviceTel;
  }

  public void setServiceTel(String serviceTel) {
    this.serviceTel = serviceTel;
  }

  public String getWxSubscribe() {
    return this.wxSubscribe;
  }

  public void setWxSubscribe(String wxSubscribe) {
    this.wxSubscribe = wxSubscribe;
  }

  public String getWxService() {
    return this.wxService;
  }

  public void setWxService(String wxService) {
    this.wxService = wxService;
  }

  public String getWeibo() {
    return this.weibo;
  }

  public void setWeibo(String weibo) {
    this.weibo = weibo;
  }

  @Override
  public String toString() {
    return "ProductSoftware{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", productStatus=" + productStatus +
            ", productType=" + productType +
            ", name='" + name + '\'' +
            ", detail='" + detail + '\'' +
            ", brand='" + brand + '\'' +
            ", logo='" + logo + '\'' +
            ", category=" + category +
            ", categoryId=" + categoryId +
            ", company=" + company +
            ", companyId=" + companyId +
            ", updated=" + updated +
            ", copyrights=" + copyrights +
            ", tags=" + tags +
            ", url='" + url + '\'' +
            ", serviceTel='" + serviceTel + '\'' +
            ", wxSubscribe='" + wxSubscribe + '\'' +
            ", wxService='" + wxService + '\'' +
            ", weibo='" + weibo + '\'' +
            '}';
  }
}