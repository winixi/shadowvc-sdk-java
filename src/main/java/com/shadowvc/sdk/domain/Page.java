package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Page.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Page extends ApiObject {

  @ApiField
  private Long total;

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiListField
  private List<?> list;

  public Long getTotal() {
    return this.total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public List<?> getList() {
    return this.list;
  }

  public void setList(List<?> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "Page{" +
            "total=" + total +
            ", page=" + page +
            ", rows=" + rows +
            ", list=" + list +
            '}';
  }
}