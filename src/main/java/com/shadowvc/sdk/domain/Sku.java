package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: Sku.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Sku extends ApiObject {

  @ApiField
  private Integer productId;

  @ApiField
  private Integer price;

  @ApiField
  private String name;

  @ApiField
  private String describe;

  public Integer getProductId() {
    return this.productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public Integer getPrice() {
    return this.price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescribe() {
    return this.describe;
  }

  public void setDescribe(String describe) {
    this.describe = describe;
  }

  @Override
  public String toString() {
    return "Sku{" +
            "productId=" + productId +
            ", price=" + price +
            ", name='" + name + '\'' +
            ", describe='" + describe + '\'' +
            '}';
  }
}