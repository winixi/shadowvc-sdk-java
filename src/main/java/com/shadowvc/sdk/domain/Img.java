package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Img.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Img extends ApiObject {

  @ApiField
  private Integer companyId;

  @ApiField
  private String wxOpenId;

  @ApiField
  private Integer memberId;

  @ApiField
  private String storageKey;

  @ApiField
  private String name;

  @ApiField
  private String format;

  @ApiField
  private Long size;

  @ApiField
  private String url;

  @ApiListField
  private List<String> tags;

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public Integer getMemberId() {
    return this.memberId;
  }

  public void setMemberId(Integer memberId) {
    this.memberId = memberId;
  }

  public String getStorageKey() {
    return this.storageKey;
  }

  public void setStorageKey(String storageKey) {
    this.storageKey = storageKey;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFormat() {
    return this.format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public Long getSize() {
    return this.size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return "Img{" +
            "companyId=" + companyId +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", memberId=" + memberId +
            ", storageKey='" + storageKey + '\'' +
            ", name='" + name + '\'' +
            ", format='" + format + '\'' +
            ", size=" + size +
            ", url='" + url + '\'' +
            ", tags=" + tags +
            '}';
  }
}