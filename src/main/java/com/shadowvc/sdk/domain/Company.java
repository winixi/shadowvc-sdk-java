package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.domain.Area;
import com.shadowvc.sdk.domain.Industry;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.Date;


/**
 * 领域模型
 * 
 * File: Company.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Company extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String name;

  @ApiField
  private String address;

  @ApiField
  private String telephone;

  @ApiField
  private String fax;

  @ApiField
  private String email;

  @ApiField
  private String homepage;

  @ApiField
  private String linkman;

  @ApiField
  private String linkphone;

  @ApiField
  private Area area;

  @ApiField
  private String areaCode;

  @ApiField
  private Industry industry;

  @ApiField
  private Integer industryId;

  @ApiField
  private Date updated;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String logo;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getTelephone() {
    return this.telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getFax() {
    return this.fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getHomepage() {
    return this.homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public String getLinkman() {
    return this.linkman;
  }

  public void setLinkman(String linkman) {
    this.linkman = linkman;
  }

  public String getLinkphone() {
    return this.linkphone;
  }

  public void setLinkphone(String linkphone) {
    this.linkphone = linkphone;
  }

  public Area getArea() {
    return this.area;
  }

  public void setArea(Area area) {
    this.area = area;
  }

  public String getAreaCode() {
    return this.areaCode;
  }

  public void setAreaCode(String areaCode) {
    this.areaCode = areaCode;
  }

  public Industry getIndustry() {
    return this.industry;
  }

  public void setIndustry(Industry industry) {
    this.industry = industry;
  }

  public Integer getIndustryId() {
    return this.industryId;
  }

  public void setIndustryId(Integer industryId) {
    this.industryId = industryId;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  @Override
  public String toString() {
    return "Company{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", telephone='" + telephone + '\'' +
            ", fax='" + fax + '\'' +
            ", email='" + email + '\'' +
            ", homepage='" + homepage + '\'' +
            ", linkman='" + linkman + '\'' +
            ", linkphone='" + linkphone + '\'' +
            ", area=" + area +
            ", areaCode='" + areaCode + '\'' +
            ", industry=" + industry +
            ", industryId=" + industryId +
            ", updated=" + updated +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", logo='" + logo + '\'' +
            '}';
  }
}