package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.ShowStatus;
import com.shadowvc.sdk.domain.Category;
import com.shadowvc.sdk.domain.Tag;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.Date;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Category.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Category extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private Integer parentId;

  @ApiField
  private Integer sort;

  @ApiField
  private String name;

  @ApiField
  private String logo;

  @ApiField
  private String label;

  @ApiField
  private ShowStatus showStatus;

  @ApiField
  private String memo;

  @ApiField
  private Date created;

  @ApiField
  private Date updated;

  @ApiListField
  private List<Tag> tags;

  @ApiListField
  private List<Category> subset;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getParentId() {
    return this.parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Integer getSort() {
    return this.sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public ShowStatus getShowStatus() {
    return this.showStatus;
  }

  public void setShowStatus(ShowStatus showStatus) {
    this.showStatus = showStatus;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public List<Tag> getTags() {
    return this.tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  public List<Category> getSubset() {
    return this.subset;
  }

  public void setSubset(List<Category> subset) {
    this.subset = subset;
  }

  @Override
  public String toString() {
    return "Category{" +
            "id=" + id +
            ", parentId=" + parentId +
            ", sort=" + sort +
            ", name='" + name + '\'' +
            ", logo='" + logo + '\'' +
            ", label='" + label + '\'' +
            ", showStatus=" + showStatus +
            ", memo='" + memo + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            ", tags=" + tags +
            ", subset=" + subset +
            '}';
  }
}