package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.domain.Area;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Area.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Area extends ApiObject {

  @ApiField
  private String code;

  @ApiField
  private String name;

  @ApiListField
  private List<Area> subset;

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Area> getSubset() {
    return this.subset;
  }

  public void setSubset(List<Area> subset) {
    this.subset = subset;
  }

  @Override
  public String toString() {
    return "Area{" +
            "code='" + code + '\'' +
            ", name='" + name + '\'' +
            ", subset=" + subset +
            '}';
  }
}