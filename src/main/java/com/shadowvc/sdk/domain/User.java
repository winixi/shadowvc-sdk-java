package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.Gender;
import com.shadowvc.sdk.dict.UserStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: User.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class User extends ApiObject {

  @ApiField
  private String name;

  @ApiField
  private String mobile;

  @ApiField
  private String email;

  @ApiField
  private UserStatus userStatus;

  @ApiField
  private Gender gender;

  @ApiField
  private String telephone;

  @ApiField
  private String address;

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserStatus getUserStatus() {
    return this.userStatus;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public Gender getGender() {
    return this.gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public String getTelephone() {
    return this.telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "User{" +
            "name='" + name + '\'' +
            ", mobile='" + mobile + '\'' +
            ", email='" + email + '\'' +
            ", userStatus=" + userStatus +
            ", gender=" + gender +
            ", telephone='" + telephone + '\'' +
            ", address='" + address + '\'' +
            '}';
  }
}