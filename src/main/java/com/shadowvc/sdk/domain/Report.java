package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.ProcessStatus;
import com.shadowvc.sdk.domain.Attachment;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.Date;
import java.util.List;


/**
 * 领域模型
 * 
 * File: Report.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class Report extends ApiObject {

  @ApiField
  private String label;

  @ApiField
  private String outId;

  @ApiField
  private Integer memberId;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String name;

  @ApiField
  private String email;

  @ApiField
  private String mobile;

  @ApiField
  private String content;

  @ApiField
  private String process;

  @ApiField
  private ProcessStatus processStatus;

  @ApiListField
  private List<Attachment> attachments;

  @ApiField
  private Date created;

  @ApiField
  private Date updated;

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

  public Integer getMemberId() {
    return this.memberId;
  }

  public void setMemberId(Integer memberId) {
    this.memberId = memberId;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getProcess() {
    return this.process;
  }

  public void setProcess(String process) {
    this.process = process;
  }

  public ProcessStatus getProcessStatus() {
    return this.processStatus;
  }

  public void setProcessStatus(ProcessStatus processStatus) {
    this.processStatus = processStatus;
  }

  public List<Attachment> getAttachments() {
    return this.attachments;
  }

  public void setAttachments(List<Attachment> attachments) {
    this.attachments = attachments;
  }

  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  @Override
  public String toString() {
    return "Report{" +
            "label='" + label + '\'' +
            ", outId='" + outId + '\'' +
            ", memberId=" + memberId +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", mobile='" + mobile + '\'' +
            ", content='" + content + '\'' +
            ", process='" + process + '\'' +
            ", processStatus=" + processStatus +
            ", attachments=" + attachments +
            ", created=" + created +
            ", updated=" + updated +
            '}';
  }
}