package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.AuthStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.Date;


/**
 * 领域模型
 * 
 * File: CompanyAuth.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuth extends ApiObject {

  @ApiField
  private Integer id;

  @ApiField
  private String companyName;

  @ApiField
  private String certPic;

  @ApiField
  private String certNumber;

  @ApiField
  private String authPic;

  @ApiField
  private AuthStatus authStatus;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String mobile;

  @ApiField
  private String name;

  @ApiField
  private String reason;

  @ApiField
  private Date created;

  @ApiField
  private Date updated;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCompanyName() {
    return this.companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getCertPic() {
    return this.certPic;
  }

  public void setCertPic(String certPic) {
    this.certPic = certPic;
  }

  public String getCertNumber() {
    return this.certNumber;
  }

  public void setCertNumber(String certNumber) {
    this.certNumber = certNumber;
  }

  public String getAuthPic() {
    return this.authPic;
  }

  public void setAuthPic(String authPic) {
    this.authPic = authPic;
  }

  public AuthStatus getAuthStatus() {
    return this.authStatus;
  }

  public void setAuthStatus(AuthStatus authStatus) {
    this.authStatus = authStatus;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  @Override
  public String toString() {
    return "CompanyAuth{" +
            "id=" + id +
            ", companyName='" + companyName + '\'' +
            ", certPic='" + certPic + '\'' +
            ", certNumber='" + certNumber + '\'' +
            ", authPic='" + authPic + '\'' +
            ", authStatus=" + authStatus +
            ", wxOpenId='" + wxOpenId + '\'' +
            ", mobile='" + mobile + '\'' +
            ", name='" + name + '\'' +
            ", reason='" + reason + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            '}';
  }
}