package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 领域模型
 * 
 * File: ProductAccessCount.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductAccessCount extends ApiObject {

  @ApiField
  private Integer year;

  @ApiField
  private Integer week;

  @ApiField
  private Integer day1;

  @ApiField
  private Integer day2;

  @ApiField
  private Integer day3;

  @ApiField
  private Integer day4;

  @ApiField
  private Integer day5;

  @ApiField
  private Integer day6;

  @ApiField
  private Integer day7;

  @ApiField
  private Integer count;

  public Integer getYear() {
    return this.year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Integer getWeek() {
    return this.week;
  }

  public void setWeek(Integer week) {
    this.week = week;
  }

  public Integer getDay1() {
    return this.day1;
  }

  public void setDay1(Integer day1) {
    this.day1 = day1;
  }

  public Integer getDay2() {
    return this.day2;
  }

  public void setDay2(Integer day2) {
    this.day2 = day2;
  }

  public Integer getDay3() {
    return this.day3;
  }

  public void setDay3(Integer day3) {
    this.day3 = day3;
  }

  public Integer getDay4() {
    return this.day4;
  }

  public void setDay4(Integer day4) {
    this.day4 = day4;
  }

  public Integer getDay5() {
    return this.day5;
  }

  public void setDay5(Integer day5) {
    this.day5 = day5;
  }

  public Integer getDay6() {
    return this.day6;
  }

  public void setDay6(Integer day6) {
    this.day6 = day6;
  }

  public Integer getDay7() {
    return this.day7;
  }

  public void setDay7(Integer day7) {
    this.day7 = day7;
  }

  public Integer getCount() {
    return this.count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  @Override
  public String toString() {
    return "ProductAccessCount{" +
            "year=" + year +
            ", week=" + week +
            ", day1=" + day1 +
            ", day2=" + day2 +
            ", day3=" + day3 +
            ", day4=" + day4 +
            ", day5=" + day5 +
            ", day6=" + day6 +
            ", day7=" + day7 +
            ", count=" + count +
            '}';
  }
}