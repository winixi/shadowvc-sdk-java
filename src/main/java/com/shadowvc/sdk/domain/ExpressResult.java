package com.shadowvc.sdk.domain;

import com.shadowvc.sdk.internal.util.ApiObject;
import com.shadowvc.sdk.dict.ExpressAbbr;
import com.shadowvc.sdk.dict.ExpressStatus;
import com.shadowvc.sdk.domain.ExpressRecord;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.Date;
import java.util.List;


/**
 * 领域模型
 * 
 * File: ExpressResult.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ExpressResult extends ApiObject {

  @ApiListField
  private List<ExpressRecord> records;

  @ApiField
  private ExpressAbbr express;

  @ApiField
  private String number;

  @ApiField
  private Date updated;

  @ApiField
  private ExpressStatus status;

  @ApiField
  private String message;

  @ApiField
  private String tel;

  public List<ExpressRecord> getRecords() {
    return this.records;
  }

  public void setRecords(List<ExpressRecord> records) {
    this.records = records;
  }

  public ExpressAbbr getExpress() {
    return this.express;
  }

  public void setExpress(ExpressAbbr express) {
    this.express = express;
  }

  public String getNumber() {
    return this.number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Date getUpdated() {
    return this.updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public ExpressStatus getStatus() {
    return this.status;
  }

  public void setStatus(ExpressStatus status) {
    this.status = status;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getTel() {
    return this.tel;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }

  @Override
  public String toString() {
    return "ExpressResult{" +
            "records=" + records +
            ", express=" + express +
            ", number='" + number + '\'' +
            ", updated=" + updated +
            ", status=" + status +
            ", message='" + message + '\'' +
            ", tel='" + tel + '\'' +
            '}';
  }
}