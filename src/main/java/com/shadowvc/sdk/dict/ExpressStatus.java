package com.shadowvc.sdk.dict;

import com.shadowvc.sdk.internal.util.ApiEnum;

/**
 * 枚举类
 * 
 * File: ExpressStatus.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public enum ExpressStatus implements ApiEnum {

  FAILED("查询失败"),
  NORMAL("正常"),
  DELIVERY("派送中"),
  RECEIVED("已签收"),
  SENDBACK("退回"),
  OTHER("其他问题");

  private String name;

  ExpressStatus(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }
}