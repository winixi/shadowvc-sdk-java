package com.shadowvc.sdk.dict;

import com.shadowvc.sdk.internal.util.ApiEnum;

/**
 * 枚举类
 * 
 * File: PraiseType.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public enum PraiseType implements ApiEnum {

  COMMENT("留言");

  private String name;

  PraiseType(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }
}