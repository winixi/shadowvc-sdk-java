package com.shadowvc.sdk.dict;

import com.shadowvc.sdk.internal.util.ApiEnum;

/**
 * 枚举类
 * 
 * File: ExpressAbbr.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public enum ExpressAbbr implements ApiEnum {

  AAE("AAE快递"),
  ANJIE("安捷快递"),
  ANXINDA("安信达快递"),
  ARAMEX("Aramex国际快递"),
  BAIQIAN("百千诚国际物流"),
  BALUNZHI("巴伦支"),
  BAOTONGDA("宝通达"),
  BENTENG("成都奔腾国际快递"),
  CCES("CCES快递"),
  CHANGTONG("长通物流"),
  CHENGGUANG("程光快递"),
  CHENGJI("城际快递"),
  CHENGSHI100("城市100"),
  CHUANXI("传喜快递"),
  CHUANZHI("传志快递"),
  CHUKOUYI("出口易物流"),
  CITYLINK("CityLinkExpress"),
  COE("东方快递"),
  CSZX("城市之星"),
  DATIAN("大田物流"),
  DAYANG("大洋物流快递"),
  DEBANG("德邦物流"),
  DECHUANG("德创物流"),
  DHL("DHL快递"),
  DIANTONG("店通快递"),
  DIDA("递达快递"),
  DINGDONG("叮咚快递"),
  DISIFANG("递四方速递"),
  DPEX("DPEX快递"),
  DSU("D速快递"),
  EES("百福东方物流"),
  EMS("EMS快递"),
  FANYU("凡宇快递"),
  FARDAR("Fardar"),
  FEDEX("国际Fedex"),
  FEDEXCN("Fedex国内"),
  FEIBANG("飞邦物流"),
  FEIBAO("飞豹快递"),
  FEIHANG("原飞航物流"),
  FEIHU("飞狐快递"),
  FEITE("飞特物流"),
  FEIYUAN("飞远物流"),
  FENGDA("丰达快递"),
  FKD("飞康达快递"),
  GDYZ("广东邮政物流"),
  GNXB("邮政国内小包"),
  GONGSUDA("共速达物流|快递"),
  GUOTONG("国通快递"),
  HAIHONG("山东海红快递"),
  HAIMENG("海盟速递"),
  HAOSHENG("昊盛物流"),
  HEBEIJIANHUA("河北建华快递"),
  HENGLU("恒路物流"),
  HUACHENG("华诚物流"),
  HUAHAN("华翰物流"),
  HUAQI("华企快递"),
  HUAXIALONG("华夏龙物流"),
  HUAYU("天地华宇物流"),
  HUIQIANG("汇强快递"),
  HUITONG("汇通快递"),
  HWHQ("海外环球快递"),
  JIAJI("佳吉快运"),
  JIAYI("佳怡物流"),
  JIAYUNMEI("加运美快递"),
  JINDA("金大物流"),
  JINGDONG("京东快递"),
  JINGGUANG("京广快递"),
  JINYUE("晋越快递"),
  JIXIANDA("急先达物流"),
  JLDT("嘉里大通物流"),
  KANGLI("康力物流"),
  KCS("顺鑫(KCS)快递"),
  KUAIJIE("快捷快递"),
  KUANRONG("宽容物流"),
  KUAYUE("跨越快递"),
  LEJIEDI("乐捷递快递"),
  LIANHAOTONG("联昊通快递"),
  LIJISONG("成都立即送快递"),
  LONGBANG("龙邦快递"),
  MINBANG("民邦快递"),
  MINGLIANG("明亮物流"),
  MINSHENG("闽盛快递"),
  NELL("尼尔快递"),
  NENGDA("港中能达快递"),
  OCS("OCS快递"),
  PINGANDA("平安达"),
  PINGYOU("中国邮政平邮"),
  PINSU("品速心达快递"),
  QUANCHEN("全晨快递"),
  QUANFENG("全峰快递"),
  QUANJITONG("全际通快递"),
  QUANRITONG("全日通快递"),
  QUANYI("全一快递"),
  RPX("RPX保时达"),
  RUFENG("如风达快递"),
  SAIAODI("赛澳递"),
  SANTAI("三态速递"),
  SCS("伟邦(SCS)快递"),
  SHENGAN("圣安物流"),
  SHENGBANG("晟邦物流"),
  SHENGFENG("盛丰物流"),
  SHENGHUI("盛辉物流"),
  SHENTONG("申通快递（可能存在延迟）"),
  SHUNFENG("顺丰快递"),
  SUCHENGZHAIPEI("速呈宅配"),
  SUIJIA("穗佳物流"),
  SURE("速尔快递"),
  TIANTIAN("天天快递"),
  TNT("TNT快递"),
  TONGCHENG("通成物流"),
  TONGHE("通和天下物流"),
  UPS("UPS快递"),
  USPS("USPS快递"),
  WANBO("万博快递"),
  WANJIA("万家物流"),
  WEITEPAI("微特派"),
  XIANGLONG("祥龙运通快递"),
  XINBANG("新邦物流"),
  XINFENG("信丰快递"),
  XINGCHENGZHAIPEI("星程宅配快递"),
  XIYOUTE("希优特快递"),
  YAD("源安达快递"),
  YAFENG("亚风快递"),
  YIBANG("一邦快递"),
  YINJIE("银捷快递"),
  YINSU("音素快运"),
  YISHUNHANG("亿顺航快递"),
  YOUSU("优速快递"),
  YTFH("北京一统飞鸿快递"),
  YUANCHENG("远成物流"),
  YUANTONG("圆通快递"),
  YUANZHI("元智捷诚"),
  YUEFENG("越丰快递"),
  YUMEIJIE("誉美捷快递"),
  YUNDA("韵达快递"),
  YUNTONG("运通中港快递"),
  YUXIN("宇鑫物流"),
  YWFEX("源伟丰"),
  ZENGYI("增益快递"),
  ZHAIJISONG("宅急送快递"),
  ZHENGZHOUJIANHUA("郑州建华快递"),
  ZHIMA("芝麻开门快递"),
  ZHONGTIAN("济南中天万运"),
  ZHONGTIE("中铁快运"),
  ZHONGTONG("中通快递"),
  ZHONGXINDA("忠信达快递"),
  ZHONGYOU("中邮物流");

  private String name;

  ExpressAbbr(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }
}