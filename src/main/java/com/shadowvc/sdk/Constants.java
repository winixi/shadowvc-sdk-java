package com.shadowvc.sdk;

/**
 * 公用常量
 * <p>
 * File: Constants.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public abstract class Constants {

  /**
   * TOP默认时间格式
   **/
  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

  /**
   * TOP Date默认时区
   **/
  public static final String DATE_TIMEZONE = "GMT+8";

  /**
   * UTF-8字符集
   **/
  public static final String CHARSET_UTF8 = "UTF-8";

  /**
   * GBK字符集
   **/
  public static final String CHARSET_GBK = "GBK";

  /**
   * TOP JSON 应格式
   */
  public static final String FORMAT_JSON = "json";

  /**
   * MD5签名方式
   */
  public static final String SIGN_METHOD_MD5 = "md5";

  /**
   * HMAC签名方式
   */
  public static final String SIGN_METHOD_HMAC = "hmac";

  /**
   * SDK版本号
   */
  public static final String SDK_VERSION = "eop-sdk-java-20140525";

}
