package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Problem;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ProblemGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProblemGetResponse extends ApiResponse {

  @ApiField
  private Problem problem;

  public Problem getProblem() {
    return this.problem;
  }

  public void setProblem(Problem problem) {
    this.problem = problem;
  }

  @Override
  public String toString() {
    return "ProblemGetResponse{" +
            "problem=" + problem +
            '}';
  }
}