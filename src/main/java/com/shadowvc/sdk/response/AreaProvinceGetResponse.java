package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Area;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: AreaProvinceGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class AreaProvinceGetResponse extends ApiResponse {

  @ApiListField
  private List<Area> list;

  @ApiField
  private Integer total;

  public List<Area> getList() {
    return this.list;
  }

  public void setList(List<Area> list) {
    this.list = list;
  }

  public Integer getTotal() {
    return this.total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  @Override
  public String toString() {
    return "AreaProvinceGetResponse{" +
            "list=" + list +
            ", total=" + total +
            '}';
  }
}