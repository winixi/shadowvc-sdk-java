package com.shadowvc.sdk.response;

import com.shadowvc.sdk.domain.OrderWx;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiResponse;


public class OrderWxRefundResponse extends ApiResponse {

  @Override
  public String toString() {
    return "OrderWxRefundResponse{" +
            '}';
  }
}
