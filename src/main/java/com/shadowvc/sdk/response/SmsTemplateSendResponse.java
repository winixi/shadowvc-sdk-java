package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: SmsTemplateSendResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class SmsTemplateSendResponse extends ApiResponse {

  @ApiField
  private Long sid;

  public Long getSid() {
    return this.sid;
  }

  public void setSid(Long sid) {
    this.sid = sid;
  }

  @Override
  public String toString() {
    return "SmsTemplateSendResponse{" +
            "sid=" + sid +
            '}';
  }
}