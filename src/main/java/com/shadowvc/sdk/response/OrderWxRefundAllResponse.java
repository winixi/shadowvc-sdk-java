package com.shadowvc.sdk.response;

import com.shadowvc.sdk.domain.OrderWxRefundAll;
import com.shadowvc.sdk.internal.util.ApiResponse;


public class OrderWxRefundAllResponse extends ApiResponse {
  private OrderWxRefundAll orderWxRefundAll;

  public OrderWxRefundAll getOrderWxRefundAll() {
    return orderWxRefundAll;
  }

  public void setOrderWxRefundAll(OrderWxRefundAll orderWxRefundAll) {
    this.orderWxRefundAll = orderWxRefundAll;
  }

  @Override
  public String toString() {
    return "OrderWxRefundAllResponse{" +
            "orderWxRefundAll=" + orderWxRefundAll +
            '}';
  }
}
