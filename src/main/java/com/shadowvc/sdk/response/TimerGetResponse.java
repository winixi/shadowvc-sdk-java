package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Timer;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: TimerGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class TimerGetResponse extends ApiResponse {

  @ApiField
  private Timer timer;

  public Timer getTimer() {
    return this.timer;
  }

  public void setTimer(Timer timer) {
    this.timer = timer;
  }

  @Override
  public String toString() {
    return "TimerGetResponse{" +
            "timer=" + timer +
            '}';
  }
}