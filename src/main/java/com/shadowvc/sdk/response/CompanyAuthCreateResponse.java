package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: CompanyAuthCreateResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuthCreateResponse extends ApiResponse {

  @ApiField
  private Integer companyAuthId;

  public Integer getCompanyAuthId() {
    return this.companyAuthId;
  }

  public void setCompanyAuthId(Integer companyAuthId) {
    this.companyAuthId = companyAuthId;
  }

  @Override
  public String toString() {
    return "CompanyAuthCreateResponse{" +
            "companyAuthId=" + companyAuthId +
            '}';
  }
}