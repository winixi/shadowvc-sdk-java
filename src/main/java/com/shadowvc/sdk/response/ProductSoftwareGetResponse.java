package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.ProductSoftware;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ProductSoftwareGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductSoftwareGetResponse extends ApiResponse {

  @ApiField
  private ProductSoftware productSoftware;

  public ProductSoftware getProductSoftware() {
    return this.productSoftware;
  }

  public void setProductSoftware(ProductSoftware productSoftware) {
    this.productSoftware = productSoftware;
  }

  @Override
  public String toString() {
    return "ProductSoftwareGetResponse{" +
            "productSoftware=" + productSoftware +
            '}';
  }
}