package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Report;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: ReportFindResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportFindResponse extends ApiResponse {

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private Long total;

  @ApiListField
  private List<Report> reports;

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public Long getTotal() {
    return this.total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public List<Report> getReports() {
    return this.reports;
  }

  public void setReports(List<Report> reports) {
    this.reports = reports;
  }

  @Override
  public String toString() {
    return "ReportFindResponse{" +
            "page=" + page +
            ", rows=" + rows +
            ", total=" + total +
            ", reports=" + reports +
            '}';
  }
}