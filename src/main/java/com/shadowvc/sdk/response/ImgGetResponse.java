package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Img;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ImgGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ImgGetResponse extends ApiResponse {

  @ApiField
  private Img img;

  public Img getImg() {
    return this.img;
  }

  public void setImg(Img img) {
    this.img = img;
  }

  @Override
  public String toString() {
    return "ImgGetResponse{" +
            "img=" + img +
            '}';
  }
}