package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.ExpressResult;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ExpressNumberGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ExpressNumberGetResponse extends ApiResponse {

  @ApiField
  private ExpressResult result;

  public ExpressResult getResult() {
    return this.result;
  }

  public void setResult(ExpressResult result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "ExpressNumberGetResponse{" +
            "result=" + result +
            '}';
  }
}