package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: SmsCodeSendResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class SmsCodeSendResponse extends ApiResponse {

  @ApiField
  private Integer smsCodeId;

  public Integer getSmsCodeId() {
    return this.smsCodeId;
  }

  public void setSmsCodeId(Integer smsCodeId) {
    this.smsCodeId = smsCodeId;
  }

  @Override
  public String toString() {
    return "SmsCodeSendResponse{" +
            "smsCodeId=" + smsCodeId +
            '}';
  }
}