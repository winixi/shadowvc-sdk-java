package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Industry;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: IndustryGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class IndustryGetResponse extends ApiResponse {

  @ApiField
  private Industry industry;

  public Industry getIndustry() {
    return this.industry;
  }

  public void setIndustry(Industry industry) {
    this.industry = industry;
  }

  @Override
  public String toString() {
    return "IndustryGetResponse{" +
            "industry=" + industry +
            '}';
  }
}