package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.CompanyAuth;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: CompanyAuthGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuthGetResponse extends ApiResponse {

  @ApiField
  private CompanyAuth companyAuth;

  public CompanyAuth getCompanyAuth() {
    return this.companyAuth;
  }

  public void setCompanyAuth(CompanyAuth companyAuth) {
    this.companyAuth = companyAuth;
  }

  @Override
  public String toString() {
    return "CompanyAuthGetResponse{" +
            "companyAuth=" + companyAuth +
            '}';
  }
}