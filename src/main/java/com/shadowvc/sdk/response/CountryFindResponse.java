package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Country;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: CountryFindResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CountryFindResponse extends ApiResponse {

  @ApiField
  private Integer total;

  @ApiListField
  private List<Country> list;

  public Integer getTotal() {
    return this.total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<Country> getList() {
    return this.list;
  }

  public void setList(List<Country> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "CountryFindResponse{" +
            "total=" + total +
            ", list=" + list +
            '}';
  }
}