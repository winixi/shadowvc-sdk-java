package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.University;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: UniversityGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class UniversityGetResponse extends ApiResponse {

  @ApiField
  private University university;

  public University getUniversity() {
    return this.university;
  }

  public void setUniversity(University university) {
    this.university = university;
  }

  @Override
  public String toString() {
    return "UniversityGetResponse{" +
            "university=" + university +
            '}';
  }
}