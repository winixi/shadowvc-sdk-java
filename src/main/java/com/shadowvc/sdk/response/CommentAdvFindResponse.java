package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Comment;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: CommentAdvFindResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CommentAdvFindResponse extends ApiResponse {

  @ApiField
  private Integer page;

  @ApiField
  private Integer pages;

  @ApiField
  private Integer rows;

  @ApiField
  private Long total;

  @ApiListField
  private List<Comment> list;

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPages() {
    return this.pages;
  }

  public void setPages(Integer pages) {
    this.pages = pages;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public Long getTotal() {
    return this.total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public List<Comment> getList() {
    return this.list;
  }

  public void setList(List<Comment> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "CommentAdvFindResponse{" +
            "page=" + page +
            ", pages=" + pages +
            ", rows=" + rows +
            ", total=" + total +
            ", list=" + list +
            '}';
  }
}