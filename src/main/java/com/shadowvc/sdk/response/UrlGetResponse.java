package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Url;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: UrlGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class UrlGetResponse extends ApiResponse {

  @ApiField
  private Url url;

  public Url getUrl() {
    return this.url;
  }

  public void setUrl(Url url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return "UrlGetResponse{" +
            "url=" + url +
            '}';
  }
}