package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Version;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: VersionGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionGetResponse extends ApiResponse {

  @ApiField
  private Version version;

  public Version getVersion() {
    return this.version;
  }

  public void setVersion(Version version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return "VersionGetResponse{" +
            "version=" + version +
            '}';
  }
}