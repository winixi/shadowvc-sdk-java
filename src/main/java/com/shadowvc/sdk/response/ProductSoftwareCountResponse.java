package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ProductSoftwareCountResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductSoftwareCountResponse extends ApiResponse {

  @ApiField
  private Integer count;

  public Integer getCount() {
    return this.count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  @Override
  public String toString() {
    return "ProductSoftwareCountResponse{" +
            "count=" + count +
            '}';
  }
}