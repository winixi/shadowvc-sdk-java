package com.shadowvc.sdk.response;

import com.shadowvc.sdk.domain.WechatPayJsapi;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiObjectField;
import com.shadowvc.sdk.internal.util.ApiResponse;


public class OrderWxJsapiMakeResponse extends ApiResponse {

  @ApiObjectField
  private WechatPayJsapi wechatPayJsapi;

  public WechatPayJsapi getWechatPayJsapi() {
    return wechatPayJsapi;
  }

  public void setWechatPayJsapi(WechatPayJsapi wechatPayJsapi) {
    this.wechatPayJsapi = wechatPayJsapi;
  }
}
