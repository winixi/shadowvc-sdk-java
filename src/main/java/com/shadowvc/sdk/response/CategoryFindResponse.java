package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Category;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: CategoryFindResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CategoryFindResponse extends ApiResponse {

  @ApiField
  private Integer total;

  @ApiListField
  private List<Category> list;

  public Integer getTotal() {
    return this.total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<Category> getList() {
    return this.list;
  }

  public void setList(List<Category> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "CategoryFindResponse{" +
            "total=" + total +
            ", list=" + list +
            '}';
  }
}