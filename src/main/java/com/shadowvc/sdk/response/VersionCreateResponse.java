package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: VersionCreateResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionCreateResponse extends ApiResponse {

  @ApiField
  private Integer versionId;

  public Integer getVersionId() {
    return this.versionId;
  }

  public void setVersionId(Integer versionId) {
    this.versionId = versionId;
  }

  @Override
  public String toString() {
    return "VersionCreateResponse{" +
            "versionId=" + versionId +
            '}';
  }
}