package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Qr;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: QrDataGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class QrDataGetResponse extends ApiResponse {

  @ApiField
  private Qr qr;

  public Qr getQr() {
    return this.qr;
  }

  public void setQr(Qr qr) {
    this.qr = qr;
  }

  @Override
  public String toString() {
    return "QrDataGetResponse{" +
            "qr=" + qr +
            '}';
  }
}