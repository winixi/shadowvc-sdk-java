package com.shadowvc.sdk.response;

import com.shadowvc.sdk.domain.OrderWx;
import com.shadowvc.sdk.domain.Problem;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiResponse;


public class OrderWxGetResponse extends ApiResponse {

  @ApiField
  private OrderWx orderWx;

  public OrderWx getOrderWx() {
    return orderWx;
  }

  public void setOrderWx(OrderWx orderWx) {
    this.orderWx = orderWx;
  }

  @Override
  public String toString() {
    return "OrderWxGetResponse{" +
            "orderWx=" + orderWx +
            '}';
  }
}
