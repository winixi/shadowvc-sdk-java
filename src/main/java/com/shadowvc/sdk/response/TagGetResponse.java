package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Tag;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: TagGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class TagGetResponse extends ApiResponse {

  @ApiField
  private Tag tag;

  public Tag getTag() {
    return this.tag;
  }

  public void setTag(Tag tag) {
    this.tag = tag;
  }

  @Override
  public String toString() {
    return "TagGetResponse{" +
            "tag=" + tag +
            '}';
  }
}