package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: CommentCreateResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CommentCreateResponse extends ApiResponse {

  @ApiField
  private Integer commentId;

  public Integer getCommentId() {
    return this.commentId;
  }

  public void setCommentId(Integer commentId) {
    this.commentId = commentId;
  }

  @Override
  public String toString() {
    return "CommentCreateResponse{" +
            "commentId=" + commentId +
            '}';
  }
}