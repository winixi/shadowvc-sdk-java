package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Agreement;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: AgreementGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class AgreementGetResponse extends ApiResponse {

  @ApiField
  private Agreement agreement;

  public Agreement getAgreement() {
    return this.agreement;
  }

  public void setAgreement(Agreement agreement) {
    this.agreement = agreement;
  }

  @Override
  public String toString() {
    return "AgreementGetResponse{" +
            "agreement=" + agreement +
            '}';
  }
}