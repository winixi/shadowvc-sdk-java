package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.ProductAccessLog;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: ProductAccessLogResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductAccessLogResponse extends ApiResponse {

  @ApiListField
  private List<ProductAccessLog> productAccessLogs;

  @ApiField
  private Integer count;

  public List<ProductAccessLog> getProductAccessLogs() {
    return this.productAccessLogs;
  }

  public void setProductAccessLogs(List<ProductAccessLog> productAccessLogs) {
    this.productAccessLogs = productAccessLogs;
  }

  public Integer getCount() {
    return this.count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  @Override
  public String toString() {
    return "ProductAccessLogResponse{" +
            "productAccessLogs=" + productAccessLogs +
            ", count=" + count +
            '}';
  }
}