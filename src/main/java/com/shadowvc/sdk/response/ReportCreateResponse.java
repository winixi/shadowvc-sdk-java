package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ReportCreateResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportCreateResponse extends ApiResponse {

  @ApiField
  private Integer reportId;

  public Integer getReportId() {
    return this.reportId;
  }

  public void setReportId(Integer reportId) {
    this.reportId = reportId;
  }

  @Override
  public String toString() {
    return "ReportCreateResponse{" +
            "reportId=" + reportId +
            '}';
  }
}