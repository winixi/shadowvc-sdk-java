package com.shadowvc.sdk.response;

import com.shadowvc.sdk.domain.WechatPayJsapi;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiObjectField;
import com.shadowvc.sdk.internal.util.ApiResponse;


public class OrderWxCreateResponse extends ApiResponse {

  @ApiField
  private String no;
 

  @ApiObjectField
  private WechatPayJsapi wechatPayJsapi;


  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public WechatPayJsapi getWechatPayJsapi() {
    return wechatPayJsapi;
  }

  public void setWechatPayJsapi(WechatPayJsapi wechatPayJsapi) {
    this.wechatPayJsapi = wechatPayJsapi;
  }
}
