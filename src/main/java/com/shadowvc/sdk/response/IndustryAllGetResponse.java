package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Industry;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: IndustryAllGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class IndustryAllGetResponse extends ApiResponse {

  @ApiField
  private Integer total;

  @ApiListField
  private List<Industry> list;

  public Integer getTotal() {
    return this.total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<Industry> getList() {
    return this.list;
  }

  public void setList(List<Industry> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "IndustryAllGetResponse{" +
            "total=" + total +
            ", list=" + list +
            '}';
  }
}