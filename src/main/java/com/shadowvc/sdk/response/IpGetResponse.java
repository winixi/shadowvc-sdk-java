package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Ip;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: IpGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class IpGetResponse extends ApiResponse {

  @ApiField
  private Ip ip;

  public Ip getIp() {
    return this.ip;
  }

  public void setIp(Ip ip) {
    this.ip = ip;
  }

  @Override
  public String toString() {
    return "IpGetResponse{" +
            "ip=" + ip +
            '}';
  }
}