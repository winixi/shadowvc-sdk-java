package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ProblemModifyResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProblemModifyResponse extends ApiResponse {

  @ApiField
  private Integer result;

  public Integer getResult() {
    return this.result;
  }

  public void setResult(Integer result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "ProblemModifyResponse{" +
            "result=" + result +
            '}';
  }
}