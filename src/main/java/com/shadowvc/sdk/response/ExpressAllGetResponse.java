package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Express;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求返回模型
 * 
 * File: ExpressAllGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ExpressAllGetResponse extends ApiResponse {

  @ApiListField
  private List<Express> expresses;

  public List<Express> getExpresses() {
    return this.expresses;
  }

  public void setExpresses(List<Express> expresses) {
    this.expresses = expresses;
  }

  @Override
  public String toString() {
    return "ExpressAllGetResponse{" +
            "expresses=" + expresses +
            '}';
  }
}