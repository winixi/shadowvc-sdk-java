package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: TagCreateResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class TagCreateResponse extends ApiResponse {

  @ApiField
  private Integer tagId;

  public Integer getTagId() {
    return this.tagId;
  }

  public void setTagId(Integer tagId) {
    this.tagId = tagId;
  }

  @Override
  public String toString() {
    return "TagCreateResponse{" +
            "tagId=" + tagId +
            '}';
  }
}