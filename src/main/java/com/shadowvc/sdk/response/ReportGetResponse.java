package com.shadowvc.sdk.response;

import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.domain.Report;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求返回模型
 * 
 * File: ReportGetResponse.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportGetResponse extends ApiResponse {

  @ApiField
  private Report report;

  public Report getReport() {
    return this.report;
  }

  public void setReport(Report report) {
    this.report = report;
  }

  @Override
  public String toString() {
    return "ReportGetResponse{" +
            "report=" + report +
            '}';
  }
}