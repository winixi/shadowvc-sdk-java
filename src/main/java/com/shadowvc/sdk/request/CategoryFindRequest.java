package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CategoryFindResponse;
import com.shadowvc.sdk.dict.ShowStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CategoryFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CategoryFindRequest extends ApiRequest<CategoryFindResponse> {

  @ApiField
  private Integer parentId;

  @ApiField
  private String label;

  @ApiListField
  private List<ShowStatus> showStatuses;

  @ApiListField
  private List<String> fields;

  @Override
  public String getApiMethodName() {
    return "sd.category.find";
  }

  @Override
  public Class<CategoryFindResponse> getResponseClass() {
    return CategoryFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("parentId", parentId);
    params.put("label", label);
    params.put("showStatuses", showStatuses);
    params.put("fields", fields);
    return params;
  }

  public Integer getParentId() {
    return this.parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public List<ShowStatus> getShowStatuses() {
    return this.showStatuses;
  }

  public void setShowStatuses(List<ShowStatus> showStatuses) {
    this.showStatuses = showStatuses;
  }

  public List<String> getFields() {
    return this.fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

}