package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ImgRemoveResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ImgRemoveRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ImgRemoveRequest extends ApiRequest<ImgRemoveResponse> {

  @ApiField
  private Integer imgId;

  @ApiField
  private Integer companyId;

  @Override
  public String getApiMethodName() {
    return "sd.img.remove";
  }

  @Override
  public Class<ImgRemoveResponse> getResponseClass() {
    return ImgRemoveResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("imgId", imgId);
    params.put("companyId", companyId);
    return params;
  }

  public Integer getImgId() {
    return this.imgId;
  }

  public void setImgId(Integer imgId) {
    this.imgId = imgId;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

}