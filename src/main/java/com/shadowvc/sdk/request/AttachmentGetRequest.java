package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.AttachmentGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: AttachmentGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class AttachmentGetRequest extends ApiRequest<AttachmentGetResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private String storageKey;

  @ApiField
  private String pathVariable;

  @ApiField
  private Long expires;

  @Override
  public String getApiMethodName() {
    return "sd.attachment.get";
  }

  @Override
  public Class<AttachmentGetResponse> getResponseClass() {
    return AttachmentGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("storageKey", storageKey);
    params.put("pathVariable", pathVariable);
    params.put("expires", expires);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getStorageKey() {
    return this.storageKey;
  }

  public void setStorageKey(String storageKey) {
    this.storageKey = storageKey;
  }

  public String getPathVariable() {
    return this.pathVariable;
  }

  public void setPathVariable(String pathVariable) {
    this.pathVariable = pathVariable;
  }

  public Long getExpires() {
    return this.expires;
  }

  public void setExpires(Long expires) {
    this.expires = expires;
  }

}