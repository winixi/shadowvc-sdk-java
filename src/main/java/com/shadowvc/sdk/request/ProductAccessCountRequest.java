package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProductAccessCountResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ProductAccessCountRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductAccessCountRequest extends ApiRequest<ProductAccessCountResponse> {

  @ApiField
  private Integer productId;

  @ApiField
  private Integer year;

  @ApiField
  private Integer week;

  @Override
  public String getApiMethodName() {
    return "sd.product.access.count";
  }

  @Override
  public Class<ProductAccessCountResponse> getResponseClass() {
    return ProductAccessCountResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("productId", productId);
    params.put("year", year);
    params.put("week", week);
    return params;
  }

  public Integer getProductId() {
    return this.productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public Integer getYear() {
    return this.year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Integer getWeek() {
    return this.week;
  }

  public void setWeek(Integer week) {
    this.week = week;
  }

}