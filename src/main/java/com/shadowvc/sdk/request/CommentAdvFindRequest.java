package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CommentAdvFindResponse;
import com.shadowvc.sdk.dict.CommentStatus;
import com.shadowvc.sdk.dict.ReplyStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CommentAdvFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CommentAdvFindRequest extends ApiRequest<CommentAdvFindResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private String label;

  @ApiField
  private String outId;

  @ApiField
  private String keyword;

  @ApiListField
  private List<CommentStatus> commentStatus;

  @ApiListField
  private List<ReplyStatus> replyStatus;

  @Override
  public String getApiMethodName() {
    return "sd.comment.adv.find";
  }

  @Override
  public Class<CommentAdvFindResponse> getResponseClass() {
    return CommentAdvFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("page", page);
    params.put("rows", rows);
    params.put("label", label);
    params.put("outId", outId);
    params.put("keyword", keyword);
    params.put("commentStatus", commentStatus);
    params.put("replyStatus", replyStatus);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

  public String getKeyword() {
    return this.keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public List<CommentStatus> getCommentStatus() {
    return this.commentStatus;
  }

  public void setCommentStatus(List<CommentStatus> commentStatus) {
    this.commentStatus = commentStatus;
  }

  public List<ReplyStatus> getReplyStatus() {
    return this.replyStatus;
  }

  public void setReplyStatus(List<ReplyStatus> replyStatus) {
    this.replyStatus = replyStatus;
  }

}