package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.VersionModifyResponse;
import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: VersionModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionModifyRequest extends ApiRequest<VersionModifyResponse> {

  @ApiField
  private Integer versionId;

  @ApiField
  private Double ver;

  @ApiField
  private String url;

  @ApiField
  private Boolean forceUpdate;

  @ApiField
  private AppType appType;

  @ApiField
  private String memo;

  @Override
  public String getApiMethodName() {
    return "sd.version.modify";
  }

  @Override
  public Class<VersionModifyResponse> getResponseClass() {
    return VersionModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("versionId", versionId);
    params.put("ver", ver);
    params.put("url", url);
    params.put("forceUpdate", forceUpdate);
    params.put("appType", appType);
    params.put("memo", memo);
    return params;
  }

  public Integer getVersionId() {
    return this.versionId;
  }

  public void setVersionId(Integer versionId) {
    this.versionId = versionId;
  }

  public Double getVer() {
    return this.ver;
  }

  public void setVer(Double ver) {
    this.ver = ver;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getForceUpdate() {
    return this.forceUpdate;
  }

  public void setForceUpdate(Boolean forceUpdate) {
    this.forceUpdate = forceUpdate;
  }

  public AppType getAppType() {
    return this.appType;
  }

  public void setAppType(AppType appType) {
    this.appType = appType;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

}