package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.QrUrlGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: QrUrlGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class QrUrlGetRequest extends ApiRequest<QrUrlGetResponse> {

  @ApiField
  private String url;

  @ApiField
  private Integer size;

  @ApiField
  private Integer margin;

  @ApiField
  private String logo;

  @ApiField
  private Integer compress;

  @Override
  public String getApiMethodName() {
    return "sd.qr.url.get";
  }

  @Override
  public Class<QrUrlGetResponse> getResponseClass() {
    return QrUrlGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("url", url);
    params.put("size", size);
    params.put("margin", margin);
    params.put("logo", logo);
    params.put("compress",compress);
    return params;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getSize() {
    return this.size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getMargin() {
    return this.margin;
  }

  public void setMargin(Integer margin) {
    this.margin = margin;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public Integer getCompress() {
    return compress;
  }

  public void setCompress(Integer compress) {
    this.compress = compress;
  }
}