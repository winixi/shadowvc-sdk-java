package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.TagGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: TagGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class TagGetRequest extends ApiRequest<TagGetResponse> {

  @ApiField
  private Integer tagId;

  @Override
  public String getApiMethodName() {
    return "sd.tag.get";
  }

  @Override
  public Class<TagGetResponse> getResponseClass() {
    return TagGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("tagId", tagId);
    return params;
  }

  public Integer getTagId() {
    return this.tagId;
  }

  public void setTagId(Integer tagId) {
    this.tagId = tagId;
  }

}