package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.UniversityGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: UniversityGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class UniversityGetRequest extends ApiRequest<UniversityGetResponse> {

  @ApiField
  private Integer universityId;

  @Override
  public String getApiMethodName() {
    return "sd.university.get";
  }

  @Override
  public Class<UniversityGetResponse> getResponseClass() {
    return UniversityGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("universityId", universityId);
    return params;
  }

  public Integer getUniversityId() {
    return this.universityId;
  }

  public void setUniversityId(Integer universityId) {
    this.universityId = universityId;
  }

}