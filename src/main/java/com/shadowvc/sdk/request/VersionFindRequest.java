package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.VersionFindResponse;
import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: VersionFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionFindRequest extends ApiRequest<VersionFindResponse> {

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private String keyword;

  @ApiField
  private List<AppType> appTypes;

  @Override
  public String getApiMethodName() {
    return "sd.version.find";
  }

  @Override
  public Class<VersionFindResponse> getResponseClass() {
    return VersionFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("page", page);
    params.put("rows", rows);
    params.put("keyword", keyword);
    params.put("appTypes", appTypes);
    return params;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public String getKeyword() {
    return this.keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public List<AppType> getAppTypes() {
    return this.appTypes;
  }

  public void setAppTypes(List<AppType> appTypes) {
    this.appTypes = appTypes;
  }

}