package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.AgreementRemoveResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: AgreementRemoveRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class AgreementRemoveRequest extends ApiRequest<AgreementRemoveResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer agreementId;

  @Override
  public String getApiMethodName() {
    return "sd.agreement.remove";
  }

  @Override
  public Class<AgreementRemoveResponse> getResponseClass() {
    return AgreementRemoveResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("agreementId", agreementId);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getAgreementId() {
    return this.agreementId;
  }

  public void setAgreementId(Integer agreementId) {
    this.agreementId = agreementId;
  }

}