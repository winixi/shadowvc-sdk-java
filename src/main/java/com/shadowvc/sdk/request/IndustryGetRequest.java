package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.IndustryGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: IndustryGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class IndustryGetRequest extends ApiRequest<IndustryGetResponse> {

  @ApiField
  private Integer industryId;

  @Override
  public String getApiMethodName() {
    return "sd.industry.get";
  }

  @Override
  public Class<IndustryGetResponse> getResponseClass() {
    return IndustryGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("industryId", industryId);
    return params;
  }

  public Integer getIndustryId() {
    return this.industryId;
  }

  public void setIndustryId(Integer industryId) {
    this.industryId = industryId;
  }

}