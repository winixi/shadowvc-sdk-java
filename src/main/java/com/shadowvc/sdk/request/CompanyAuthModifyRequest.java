package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CompanyAuthModifyResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: CompanyAuthModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuthModifyRequest extends ApiRequest<CompanyAuthModifyResponse> {

  @ApiField
  private String companyName;

  @ApiField
  private String certPic;

  @ApiField
  private String certNumber;

  @ApiField
  private String authPic;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String mobile;

  @ApiField
  private String name;

  @Override
  public String getApiMethodName() {
    return "sd.company.auth.modify";
  }

  @Override
  public Class<CompanyAuthModifyResponse> getResponseClass() {
    return CompanyAuthModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyName", companyName);
    params.put("certPic", certPic);
    params.put("certNumber", certNumber);
    params.put("authPic", authPic);
    params.put("wxOpenId", wxOpenId);
    params.put("mobile", mobile);
    params.put("name", name);
    return params;
  }

  public String getCompanyName() {
    return this.companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getCertPic() {
    return this.certPic;
  }

  public void setCertPic(String certPic) {
    this.certPic = certPic;
  }

  public String getCertNumber() {
    return this.certNumber;
  }

  public void setCertNumber(String certNumber) {
    this.certNumber = certNumber;
  }

  public String getAuthPic() {
    return this.authPic;
  }

  public void setAuthPic(String authPic) {
    this.authPic = authPic;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

}