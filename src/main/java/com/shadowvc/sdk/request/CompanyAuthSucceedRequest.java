package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CompanyAuthSucceedResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: CompanyAuthSucceedRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuthSucceedRequest extends ApiRequest<CompanyAuthSucceedResponse> {

  @ApiField
  private String wxOpenId;

  @Override
  public String getApiMethodName() {
    return "sd.company.auth.succeed";
  }

  @Override
  public Class<CompanyAuthSucceedResponse> getResponseClass() {
    return CompanyAuthSucceedResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("wxOpenId", wxOpenId);
    return params;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

}