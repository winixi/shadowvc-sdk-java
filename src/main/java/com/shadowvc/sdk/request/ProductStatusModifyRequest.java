package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProductStatusModifyResponse;
import com.shadowvc.sdk.dict.ProductStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ProductStatusModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductStatusModifyRequest extends ApiRequest<ProductStatusModifyResponse> {

  @ApiField
  private Integer companyId;

  @ApiListField
  private List<Integer> productIds;

  @ApiField
  private ProductStatus productStatus;

  @Override
  public String getApiMethodName() {
    return "sd.product.status.modify";
  }

  @Override
  public Class<ProductStatusModifyResponse> getResponseClass() {
    return ProductStatusModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("productIds", productIds);
    params.put("productStatus", productStatus);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public List<Integer> getProductIds() {
    return this.productIds;
  }

  public void setProductIds(List<Integer> productIds) {
    this.productIds = productIds;
  }

  public ProductStatus getProductStatus() {
    return this.productStatus;
  }

  public void setProductStatus(ProductStatus productStatus) {
    this.productStatus = productStatus;
  }

}