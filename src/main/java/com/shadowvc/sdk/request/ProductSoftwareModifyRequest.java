package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProductSoftwareModifyResponse;
import com.shadowvc.sdk.dict.ProductStatus;
import com.shadowvc.sdk.domain.Copyright;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ProductSoftwareModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductSoftwareModifyRequest extends ApiRequest<ProductSoftwareModifyResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer productId;

  @ApiField
  private String name;

  @ApiField
  private String detail;

  @ApiField
  private String brand;

  @ApiField
  private String logo;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String url;

  @ApiField
  private Integer categoryId;

  @ApiField
  private String serviceTel;

  @ApiField
  private String wxSubscribe;

  @ApiField
  private String wxService;

  @ApiField
  private String weibo;

  @ApiListField
  private List<Copyright> copyrights;

  @ApiListField
  private List<String> tags;

  @ApiField
  private ProductStatus productStatus;

  @Override
  public String getApiMethodName() {
    return "sd.product.software.modify";
  }

  @Override
  public Class<ProductSoftwareModifyResponse> getResponseClass() {
    return ProductSoftwareModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("productId", productId);
    params.put("name", name);
    params.put("detail", detail);
    params.put("brand", brand);
    params.put("logo", logo);
    params.put("wxOpenId", wxOpenId);
    params.put("url", url);
    params.put("categoryId", categoryId);
    params.put("serviceTel", serviceTel);
    params.put("wxSubscribe", wxSubscribe);
    params.put("wxService", wxService);
    params.put("weibo", weibo);
    params.put("copyrights", copyrights);
    params.put("tags", tags);
    params.put("productStatus", productStatus);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getProductId() {
    return this.productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDetail() {
    return this.detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getBrand() {
    return this.brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public String getServiceTel() {
    return this.serviceTel;
  }

  public void setServiceTel(String serviceTel) {
    this.serviceTel = serviceTel;
  }

  public String getWxSubscribe() {
    return this.wxSubscribe;
  }

  public void setWxSubscribe(String wxSubscribe) {
    this.wxSubscribe = wxSubscribe;
  }

  public String getWxService() {
    return this.wxService;
  }

  public void setWxService(String wxService) {
    this.wxService = wxService;
  }

  public String getWeibo() {
    return this.weibo;
  }

  public void setWeibo(String weibo) {
    this.weibo = weibo;
  }

  public List<Copyright> getCopyrights() {
    return this.copyrights;
  }

  public void setCopyrights(List<Copyright> copyrights) {
    this.copyrights = copyrights;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public ProductStatus getProductStatus() {
    return this.productStatus;
  }

  public void setProductStatus(ProductStatus productStatus) {
    this.productStatus = productStatus;
  }

}