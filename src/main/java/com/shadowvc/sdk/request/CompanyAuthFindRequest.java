package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CompanyAuthFindResponse;
import com.shadowvc.sdk.dict.AuthStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CompanyAuthFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyAuthFindRequest extends ApiRequest<CompanyAuthFindResponse> {

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private String keyword;

  @ApiListField
  private List<AuthStatus> authStatuses;

  @ApiListField
  private List<String> fields;

  @Override
  public String getApiMethodName() {
    return "sd.company.auth.find";
  }

  @Override
  public Class<CompanyAuthFindResponse> getResponseClass() {
    return CompanyAuthFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("page", page);
    params.put("rows", rows);
    params.put("keyword", keyword);
    params.put("authStatuses", authStatuses);
    params.put("fields", fields);
    return params;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public String getKeyword() {
    return this.keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public List<AuthStatus> getAuthStatuses() {
    return this.authStatuses;
  }

  public void setAuthStatuses(List<AuthStatus> authStatuses) {
    this.authStatuses = authStatuses;
  }

  public List<String> getFields() {
    return this.fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

}