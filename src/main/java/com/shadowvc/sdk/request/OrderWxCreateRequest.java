package com.shadowvc.sdk.request;

import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.response.OrderWxCreateResponse;
import com.shadowvc.sdk.response.ProblemCreateResponse;


public class OrderWxCreateRequest extends ApiRequest<OrderWxCreateResponse> {

  @ApiField
  private Integer aid;
 

  @ApiField
  private String orderDesc;
 

  @ApiField
  private String orderType;
 

  @ApiField
  private Long price;
 

  @ApiField
  private String notifyUrl;
 

  @ApiField
  private String clientIp;
 

  @ApiField
  private String openId;

  @ApiField
  private Integer effective;


  @Override
  public String getApiMethodName() {
    return "sd.order.wx.create";
  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("aid", aid);
    params.put("orderDesc", orderDesc);
    params.put("orderType", orderType);
    params.put("price", price);
    params.put("notifyUrl", notifyUrl);
    params.put("clientIp", clientIp);
    params.put("openId", openId);
    params.put("effective", effective);
    return params;
  }

  @Override
  public Class<OrderWxCreateResponse> getResponseClass() {
    return OrderWxCreateResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  public Integer getAid() {
    return aid;
  }

  public void setAid(Integer aid) {
    this.aid = aid;
  }

  public String getOrderDesc() {
    return orderDesc;
  }

  public void setOrderDesc(String orderDesc) {
    this.orderDesc = orderDesc;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public String getNotifyUrl() {
    return notifyUrl;
  }

  public void setNotifyUrl(String notifyUrl) {
    this.notifyUrl = notifyUrl;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public Integer getEffective() {
    return effective;
  }

  public void setEffective(Integer effective) {
    this.effective = effective;
  }


}
