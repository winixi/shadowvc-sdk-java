package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ReportFindResponse;
import com.shadowvc.sdk.dict.ProcessStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ReportFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportFindRequest extends ApiRequest<ReportFindResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private String label;

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private String keyword;

  @ApiField
  private List<ProcessStatus> processStatuses;

  @Override
  public String getApiMethodName() {
    return "sd.report.find";
  }

  @Override
  public Class<ReportFindResponse> getResponseClass() {
    return ReportFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("label", label);
    params.put("page", page);
    params.put("rows", rows);
    params.put("keyword", keyword);
    params.put("processStatuses", processStatuses);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public String getKeyword() {
    return this.keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public List<ProcessStatus> getProcessStatuses() {
    return this.processStatuses;
  }

  public void setProcessStatuses(List<ProcessStatus> processStatuses) {
    this.processStatuses = processStatuses;
  }

}