package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CategoryModifyResponse;
import com.shadowvc.sdk.dict.ShowStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CategoryModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CategoryModifyRequest extends ApiRequest<CategoryModifyResponse> {

  @ApiField
  private Integer categoryId;

  @ApiField
  private Integer parentId;

  @ApiField
  private Integer sort;

  @ApiField
  private String name;

  @ApiField
  private String logo;

  @ApiField
  private ShowStatus showStatus;

  @ApiField
  private String memo;

  @ApiListField
  private List<String> tags;

  @Override
  public String getApiMethodName() {
    return "sd.category.modify";
  }

  @Override
  public Class<CategoryModifyResponse> getResponseClass() {
    return CategoryModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("categoryId", categoryId);
    params.put("parentId", parentId);
    params.put("sort", sort);
    params.put("name", name);
    params.put("logo", logo);
    params.put("showStatus", showStatus);
    params.put("memo", memo);
    params.put("tags", tags);
    return params;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public Integer getParentId() {
    return this.parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Integer getSort() {
    return this.sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public ShowStatus getShowStatus() {
    return this.showStatus;
  }

  public void setShowStatus(ShowStatus showStatus) {
    this.showStatus = showStatus;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

}