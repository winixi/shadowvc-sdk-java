package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CompanyModifyResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: CompanyModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CompanyModifyRequest extends ApiRequest<CompanyModifyResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private String name;

  @ApiField
  private String address;

  @ApiField
  private String telephone;

  @ApiField
  private String fax;

  @ApiField
  private String email;

  @ApiField
  private String homepage;

  @ApiField
  private String linkman;

  @ApiField
  private String linkphone;

  @ApiField
  private String areaCode;

  @ApiField
  private Integer industryId;

  @ApiField
  private String logo;

  @ApiField
  private String wxOpenId;

  @Override
  public String getApiMethodName() {
    return "sd.company.modify";
  }

  @Override
  public Class<CompanyModifyResponse> getResponseClass() {
    return CompanyModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("name", name);
    params.put("address", address);
    params.put("telephone", telephone);
    params.put("fax", fax);
    params.put("email", email);
    params.put("homepage", homepage);
    params.put("linkman", linkman);
    params.put("linkphone", linkphone);
    params.put("areaCode", areaCode);
    params.put("industryId", industryId);
    params.put("logo", logo);
    params.put("wxOpenId", wxOpenId);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getTelephone() {
    return this.telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getFax() {
    return this.fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getHomepage() {
    return this.homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public String getLinkman() {
    return this.linkman;
  }

  public void setLinkman(String linkman) {
    this.linkman = linkman;
  }

  public String getLinkphone() {
    return this.linkphone;
  }

  public void setLinkphone(String linkphone) {
    this.linkphone = linkphone;
  }

  public String getAreaCode() {
    return this.areaCode;
  }

  public void setAreaCode(String areaCode) {
    this.areaCode = areaCode;
  }

  public Integer getIndustryId() {
    return this.industryId;
  }

  public void setIndustryId(Integer industryId) {
    this.industryId = industryId;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

}