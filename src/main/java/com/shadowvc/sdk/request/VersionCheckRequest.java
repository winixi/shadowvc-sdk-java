package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.VersionCheckResponse;
import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: VersionCheckRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionCheckRequest extends ApiRequest<VersionCheckResponse> {

  @ApiField
  private Double ver;

  @ApiField
  private AppType appType;

  @Override
  public String getApiMethodName() {
    return "sd.version.check";
  }

  @Override
  public Class<VersionCheckResponse> getResponseClass() {
    return VersionCheckResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("ver", ver);
    params.put("appType", appType);
    return params;
  }

  public Double getVer() {
    return this.ver;
  }

  public void setVer(Double ver) {
    this.ver = ver;
  }

  public AppType getAppType() {
    return this.appType;
  }

  public void setAppType(AppType appType) {
    this.appType = appType;
  }

}