package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ReportCreateResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ReportCreateRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportCreateRequest extends ApiRequest<ReportCreateResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private String label;

  @ApiField
  private String outId;

  @ApiField
  private Integer memberId;

  @ApiField
  private String wxOpenId;

  @ApiField
  private String name;

  @ApiField
  private String email;

  @ApiField
  private String mobile;

  @ApiField
  private String content;

  @ApiListField
  private List<String> storageKeys;

  @Override
  public String getApiMethodName() {
    return "sd.report.create";
  }

  @Override
  public Class<ReportCreateResponse> getResponseClass() {
    return ReportCreateResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("label", label);
    params.put("outId", outId);
    params.put("memberId", memberId);
    params.put("wxOpenId", wxOpenId);
    params.put("name", name);
    params.put("email", email);
    params.put("mobile", mobile);
    params.put("content", content);
    params.put("storageKeys", storageKeys);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

  public Integer getMemberId() {
    return this.memberId;
  }

  public void setMemberId(Integer memberId) {
    this.memberId = memberId;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public List<String> getStorageKeys() {
    return this.storageKeys;
  }

  public void setStorageKeys(List<String> storageKeys) {
    this.storageKeys = storageKeys;
  }

}