package com.shadowvc.sdk.request;

import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.response.OrderWxGetResponse;


public class OrderWxGetRequest extends ApiRequest<OrderWxGetResponse>{

  @ApiField
  private Integer aid;
 

  @ApiField
  private String no;


  @Override
  public String getApiMethodName() {
    return "sd.order.wx.get";
  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("aid", aid);
    params.put("no", no);
    return params;
  }

  @Override
  public Class<OrderWxGetResponse> getResponseClass() {
    return OrderWxGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  public Integer getAid() {
    return aid;
  }

  public void setAid(Integer aid) {
    this.aid = aid;
  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }
}
