package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiUploadRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.AttachmentUploadResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiFileField;
import com.shadowvc.sdk.internal.util.FileItem;
import java.util.HashMap;
import java.util.Map;


/**
 * 请求模型
 * 
 * File: AttachmentUploadRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class AttachmentUploadRequest extends ApiUploadRequest<AttachmentUploadResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private String wxOpenId;

  @ApiField
  private Integer memberId;

  @ApiFileField
  private FileItem fileItem;

  @ApiField
  private String outId;

  @Override
  public String getApiMethodName() {
    return "sd.attachment.upload";
  }

  @Override
  public Class<AttachmentUploadResponse> getResponseClass() {
    return AttachmentUploadResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("wxOpenId", wxOpenId);
    params.put("memberId", memberId);
    params.put("outId", outId);
    return params;
  }

  @Override
  public Map<String, FileItem> getFileParams() {
    Map<String, FileItem> params = new HashMap<>();
    params.put("fileItem", fileItem);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public Integer getMemberId() {
    return this.memberId;
  }

  public void setMemberId(Integer memberId) {
    this.memberId = memberId;
  }

  public FileItem getFileItem() {
    return this.fileItem;
  }

  public void setFileItem(FileItem fileItem) {
    this.fileItem = fileItem;
  }

  public String getOutId() {
    return this.outId;
  }

  public void setOutId(String outId) {
    this.outId = outId;
  }

}