package com.shadowvc.sdk.request;

import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.response.OrderWxGetResponse;
import com.shadowvc.sdk.response.OrderWxRefundResponse;


public class OrderWxRefundRequest extends ApiRequest<OrderWxRefundResponse>{

  @ApiField
  private String no;

  @ApiField
  private Long price;


  @Override
  public String getApiMethodName() {
    return "sd.order.wx.refund";
  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("no", no);
    params.put("price", price);
    return params;
  }

  @Override
  public Class<OrderWxRefundResponse> getResponseClass() {
    return OrderWxRefundResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }
}
