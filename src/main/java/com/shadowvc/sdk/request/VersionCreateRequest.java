package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.VersionCreateResponse;
import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: VersionCreateRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionCreateRequest extends ApiRequest<VersionCreateResponse> {

  @ApiField
  private Double ver;

  @ApiField
  private String url;

  @ApiField
  private Boolean forceUpdate;

  @ApiField
  private AppType appType;

  @ApiField
  private String memo;

  @Override
  public String getApiMethodName() {
    return "sd.version.create";
  }

  @Override
  public Class<VersionCreateResponse> getResponseClass() {
    return VersionCreateResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("ver", ver);
    params.put("url", url);
    params.put("forceUpdate", forceUpdate);
    params.put("appType", appType);
    params.put("memo", memo);
    return params;
  }

  public Double getVer() {
    return this.ver;
  }

  public void setVer(Double ver) {
    this.ver = ver;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getForceUpdate() {
    return this.forceUpdate;
  }

  public void setForceUpdate(Boolean forceUpdate) {
    this.forceUpdate = forceUpdate;
  }

  public AppType getAppType() {
    return this.appType;
  }

  public void setAppType(AppType appType) {
    this.appType = appType;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

}