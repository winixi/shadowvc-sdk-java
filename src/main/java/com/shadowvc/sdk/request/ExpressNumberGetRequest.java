package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ExpressNumberGetResponse;
import com.shadowvc.sdk.dict.ExpressAbbr;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ExpressNumberGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ExpressNumberGetRequest extends ApiRequest<ExpressNumberGetResponse> {

  @ApiField
  private ExpressAbbr abbr;

  @ApiField
  private String number;

  @Override
  public String getApiMethodName() {
    return "sd.express.number.get";
  }

  @Override
  public Class<ExpressNumberGetResponse> getResponseClass() {
    return ExpressNumberGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("abbr", abbr);
    params.put("number", number);
    return params;
  }

  public ExpressAbbr getAbbr() {
    return this.abbr;
  }

  public void setAbbr(ExpressAbbr abbr) {
    this.abbr = abbr;
  }

  public String getNumber() {
    return this.number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

}