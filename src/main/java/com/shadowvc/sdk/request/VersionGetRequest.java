package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.VersionGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: VersionGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class VersionGetRequest extends ApiRequest<VersionGetResponse> {

  @ApiField
  private Integer versionId;

  @Override
  public String getApiMethodName() {
    return "sd.version.get";
  }

  @Override
  public Class<VersionGetResponse> getResponseClass() {
    return VersionGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("versionId", versionId);
    return params;
  }

  public Integer getVersionId() {
    return this.versionId;
  }

  public void setVersionId(Integer versionId) {
    this.versionId = versionId;
  }

}