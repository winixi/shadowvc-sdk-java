package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ReportRemoveResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ReportRemoveRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportRemoveRequest extends ApiRequest<ReportRemoveResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer reportId;

  @Override
  public String getApiMethodName() {
    return "sd.report.remove";
  }

  @Override
  public Class<ReportRemoveResponse> getResponseClass() {
    return ReportRemoveResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("reportId", reportId);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getReportId() {
    return this.reportId;
  }

  public void setReportId(Integer reportId) {
    this.reportId = reportId;
  }

}