package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CommentCloseResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CommentCloseRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CommentCloseRequest extends ApiRequest<CommentCloseResponse> {

  @ApiField
  private Integer companyId;

  @ApiListField
  private List<Integer> commentIds;

  @Override
  public String getApiMethodName() {
    return "sd.comment.close";
  }

  @Override
  public Class<CommentCloseResponse> getResponseClass() {
    return CommentCloseResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("commentIds", commentIds);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public List<Integer> getCommentIds() {
    return this.commentIds;
  }

  public void setCommentIds(List<Integer> commentIds) {
    this.commentIds = commentIds;
  }

}