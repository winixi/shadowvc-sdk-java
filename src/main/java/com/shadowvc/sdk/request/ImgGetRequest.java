package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ImgGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ImgGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ImgGetRequest extends ApiRequest<ImgGetResponse> {

  @ApiField
  private String storageKey;

  @ApiField
  private String pathVariable;

  @ApiField
  private Integer companyId;

  @Override
  public String getApiMethodName() {
    return "sd.img.get";
  }

  @Override
  public Class<ImgGetResponse> getResponseClass() {
    return ImgGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("storageKey", storageKey);
    params.put("pathVariable", pathVariable);
    params.put("companyId", companyId);
    return params;
  }

  public String getStorageKey() {
    return this.storageKey;
  }

  public void setStorageKey(String storageKey) {
    this.storageKey = storageKey;
  }

  public String getPathVariable() {
    return this.pathVariable;
  }

  public void setPathVariable(String pathVariable) {
    this.pathVariable = pathVariable;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

}