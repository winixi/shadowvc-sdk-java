package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CountryAllGetResponse;


/**
 * 请求模型
 * 
 * File: CountryAllGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CountryAllGetRequest extends ApiRequest<CountryAllGetResponse> {

  @Override
  public String getApiMethodName() {
    return "sd.country.all.get";
  }

  @Override
  public Class<CountryAllGetResponse> getResponseClass() {
    return CountryAllGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    return params;
  }

}