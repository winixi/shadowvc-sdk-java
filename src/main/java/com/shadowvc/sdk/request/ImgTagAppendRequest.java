package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ImgTagAppendResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ImgTagAppendRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ImgTagAppendRequest extends ApiRequest<ImgTagAppendResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer imgId;

  @ApiListField
  private List<String> tags;

  @Override
  public String getApiMethodName() {
    return "sd.img.tag.append";
  }

  @Override
  public Class<ImgTagAppendResponse> getResponseClass() {
    return ImgTagAppendResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("imgId", imgId);
    params.put("tags", tags);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getImgId() {
    return this.imgId;
  }

  public void setImgId(Integer imgId) {
    this.imgId = imgId;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

}