package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProductSoftwareFindResponse;
import com.shadowvc.sdk.dict.ProductStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ProductSoftwareFindRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProductSoftwareFindRequest extends ApiRequest<ProductSoftwareFindResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer page;

  @ApiField
  private Integer rows;

  @ApiField
  private String keyword;

  @ApiListField
  private List<String> fields;

  @ApiListField
  private List<ProductStatus> productStatus;

  @ApiField
  private Integer categoryId;

  @Override
  public String getApiMethodName() {
    return "sd.product.software.find";
  }

  @Override
  public Class<ProductSoftwareFindResponse> getResponseClass() {
    return ProductSoftwareFindResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("page", page);
    params.put("rows", rows);
    params.put("keyword", keyword);
    params.put("fields", fields);
    params.put("productStatus", productStatus);
    params.put("categoryId", categoryId);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getPage() {
    return this.page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getRows() {
    return this.rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public String getKeyword() {
    return this.keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public List<String> getFields() {
    return this.fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public List<ProductStatus> getProductStatus() {
    return this.productStatus;
  }

  public void setProductStatus(List<ProductStatus> productStatus) {
    this.productStatus = productStatus;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

}