package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProblemModifyResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ProblemModifyRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProblemModifyRequest extends ApiRequest<ProblemModifyResponse> {

  @ApiField
  private String uuid;

  @ApiField
  private String title;

  @ApiField
  private String label;

  @ApiField
  private Integer categoryId;

  @ApiField
  private String content;

  @ApiField
  private String wxOpenId;

  @ApiListField
  private List<String> tags;

  @Override
  public String getApiMethodName() {
    return "sd.problem.modify";
  }

  @Override
  public Class<ProblemModifyResponse> getResponseClass() {
    return ProblemModifyResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("uuid", uuid);
    params.put("title", title);
    params.put("label", label);
    params.put("categoryId", categoryId);
    params.put("content", content);
    params.put("wxOpenId", wxOpenId);
    params.put("tags", tags);
    return params;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Integer getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

}