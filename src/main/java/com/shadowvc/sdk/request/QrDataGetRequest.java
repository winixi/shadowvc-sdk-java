package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.QrDataGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: QrDataGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class QrDataGetRequest extends ApiRequest<QrDataGetResponse> {

  @ApiField
  private String data;

  @ApiField
  private Integer size;

  @ApiField
  private Integer margin;

  @ApiField
  private String logo;

  @ApiField
  private Integer compress;

  @Override
  public String getApiMethodName() {
    return "sd.qr.data.get";
  }

  @Override
  public Class<QrDataGetResponse> getResponseClass() {
    return QrDataGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("data", data);
    params.put("size", size);
    params.put("margin", margin);
    params.put("logo", logo);
    return params;
  }

  public String getData() {
    return this.data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Integer getSize() {
    return this.size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getMargin() {
    return this.margin;
  }

  public void setMargin(Integer margin) {
    this.margin = margin;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public Integer getCompress() {
    return compress;
  }

  public void setCompress(Integer compress) {
    this.compress = compress;
  }
}