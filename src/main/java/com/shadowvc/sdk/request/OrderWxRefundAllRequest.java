package com.shadowvc.sdk.request;

import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.response.OrderWxRefundAllResponse;
import com.shadowvc.sdk.response.OrderWxRefundResponse;


public class OrderWxRefundAllRequest extends ApiRequest<OrderWxRefundAllResponse> {

  @ApiField
  private String no;


  @Override
  public String getApiMethodName() {
    return "sd.order.wx.refund.all";
  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("no", no);
    return params;
  }

  @Override
  public Class<OrderWxRefundAllResponse> getResponseClass() {
    return OrderWxRefundAllResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

}
