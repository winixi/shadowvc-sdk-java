package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ProblemGetResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: ProblemGetRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ProblemGetRequest extends ApiRequest<ProblemGetResponse> {

  @ApiField
  private String uuid;

  @ApiListField
  private List<String> fields;

  @Override
  public String getApiMethodName() {
    return "sd.problem.get";
  }

  @Override
  public Class<ProblemGetResponse> getResponseClass() {
    return ProblemGetResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("uuid", uuid);
    params.put("fields", fields);
    return params;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public List<String> getFields() {
    return this.fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

}