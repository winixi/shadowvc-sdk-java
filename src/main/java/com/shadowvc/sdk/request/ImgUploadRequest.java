package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiUploadRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ImgUploadResponse;
import com.shadowvc.sdk.dict.StorageType;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiFileField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import com.shadowvc.sdk.internal.util.FileItem;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 请求模型
 * 
 * File: ImgUploadRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ImgUploadRequest extends ApiUploadRequest<ImgUploadResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private StorageType storageType;

  @ApiField
  private String wxOpenId;

  @ApiField
  private Integer memberId;

  @ApiListField
  private List<String> tags;

  @ApiFileField
  private FileItem fileItem;

  @Override
  public String getApiMethodName() {
    return "sd.img.upload";
  }

  @Override
  public Class<ImgUploadResponse> getResponseClass() {
    return ImgUploadResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("storageType", storageType);
    params.put("wxOpenId", wxOpenId);
    params.put("memberId", memberId);
    params.put("tags", tags);
    return params;
  }

  @Override
  public Map<String, FileItem> getFileParams() {
    Map<String, FileItem> params = new HashMap<>();
    params.put("fileItem", fileItem);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public StorageType getStorageType() {
    return this.storageType;
  }

  public void setStorageType(StorageType storageType) {
    this.storageType = storageType;
  }

  public String getWxOpenId() {
    return this.wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public Integer getMemberId() {
    return this.memberId;
  }

  public void setMemberId(Integer memberId) {
    this.memberId = memberId;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public FileItem getFileItem() {
    return this.fileItem;
  }

  public void setFileItem(FileItem fileItem) {
    this.fileItem = fileItem;
  }

}