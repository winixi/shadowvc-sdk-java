package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.ReportProcessResponse;
import com.shadowvc.sdk.internal.annotation.ApiField;


/**
 * 请求模型
 * 
 * File: ReportProcessRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class ReportProcessRequest extends ApiRequest<ReportProcessResponse> {

  @ApiField
  private Integer companyId;

  @ApiField
  private Integer reportId;

  @ApiField
  private String process;

  @Override
  public String getApiMethodName() {
    return "sd.report.process";
  }

  @Override
  public Class<ReportProcessResponse> getResponseClass() {
    return ReportProcessResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("companyId", companyId);
    params.put("reportId", reportId);
    params.put("process", process);
    return params;
  }

  public Integer getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Integer getReportId() {
    return this.reportId;
  }

  public void setReportId(Integer reportId) {
    this.reportId = reportId;
  }

  public String getProcess() {
    return this.process;
  }

  public void setProcess(String process) {
    this.process = process;
  }

}