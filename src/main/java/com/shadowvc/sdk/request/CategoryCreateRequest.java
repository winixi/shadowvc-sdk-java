package com.shadowvc.sdk.request;

import com.shadowvc.sdk.internal.util.ApiRequest;
import com.shadowvc.sdk.exception.ApiRuleException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.response.CategoryCreateResponse;
import com.shadowvc.sdk.dict.ShowStatus;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import java.util.List;


/**
 * 请求模型
 * 
 * File: CategoryCreateRequest.java
 * Description: 
 * 
 * Copyright: Copyright (c) 2016 shadowvc.com
 * Company: ShadowVC,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 * @since Tue Jul 26 18:23:26 CST 2016
 */
public class CategoryCreateRequest extends ApiRequest<CategoryCreateResponse> {

  @ApiField
  private Integer parentId;

  @ApiField
  private Integer sort;

  @ApiField
  private String name;

  @ApiField
  private String label;

  @ApiField
  private String logo;

  @ApiField
  private String memo;

  @ApiField
  private ShowStatus showStatus;

  @ApiListField
  private List<String> tags;

  @Override
  public String getApiMethodName() {
    return "sd.category.create";
  }

  @Override
  public Class<CategoryCreateResponse> getResponseClass() {
    return CategoryCreateResponse.class;
  }

  @Override
  public void check() throws ApiRuleException {

  }

  @Override
  public ApiHashMap getTextParams() {
    ApiHashMap params = new ApiHashMap();
    params.put("parentId", parentId);
    params.put("sort", sort);
    params.put("name", name);
    params.put("label", label);
    params.put("logo", logo);
    params.put("memo", memo);
    params.put("showStatus", showStatus);
    params.put("tags", tags);
    return params;
  }

  public Integer getParentId() {
    return this.parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Integer getSort() {
    return this.sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getLogo() {
    return this.logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getMemo() {
    return this.memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public ShowStatus getShowStatus() {
    return this.showStatus;
  }

  public void setShowStatus(ShowStatus showStatus) {
    this.showStatus = showStatus;
  }

  public List<String> getTags() {
    return this.tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

}