package com.shadowvc.sdk.internal.stream;

/**
 * 长连接异常类
 * <p>
 * File: EopCometException.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class EopCometException extends Exception {

  private static final long serialVersionUID = -2742310793769087686L;

  public EopCometException() {
    super();
  }

  public EopCometException(String message, Throwable cause) {
    super(message, cause);
  }

  public EopCometException(String message) {
    super(message);
  }

  public EopCometException(Throwable cause) {
    super(cause);
  }

}
