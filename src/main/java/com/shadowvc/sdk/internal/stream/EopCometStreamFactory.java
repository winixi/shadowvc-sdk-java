package com.shadowvc.sdk.internal.stream;

/**
 * 长连接工厂
 * <p>
 * File: EopCometStreamFactory.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class EopCometStreamFactory {

  private Configuration configruation;

  public EopCometStreamFactory(Configuration configuration) {
    if (configuration == null) {
      throw new RuntimeException("configuration is must not null!");
    }
    this.configruation = configuration;
  }

  /**
   * 获取一个长连接实例
   *
   * @return 返回
   */
  public EopCometStream getInstance() {
    return new EopCometStreamImpl(configruation);
  }
}
