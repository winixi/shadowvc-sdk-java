package com.shadowvc.sdk.internal.stream.connect;

import java.util.Map;

/**
 * 建立http connection需要的一些配置
 *
 * @author zhenzi 2011-8-8 下午05:56:41
 */
public interface HttpConnectionConfiguration {

  /**
   * 连接地址
   *
   * @return 返回
   */
  String getConnectUrl();

  /**
   * 连接超时时间
   *
   * @return 返回
   */
  int getHttpConnectionTimeout();

  /**
   * http读取数据超时时间
   *
   * @return 返回
   */
  int getHttpReadTimeout();

  /**
   * 连接重试次数
   *
   * @return 返回
   */
  int getHttpConnectRetryCount();

  /**
   * 重试间隔的时间
   *
   * @return 返回
   */
  int getHttpConnectRetryInterval();

  /**
   * 返回服务端在升级的时候的休眠时间
   *
   * @return 返回
   */
  int getSleepTimeOfServerInUpgrade();

  /**
   * 对于一个连接重连间隔的时间
   *
   * @return 返回
   */
  int getHttpReconnectInterval();

  /**
   * @return 返回
   */
  Map<String, String> getRequestHeader();
}
