package com.shadowvc.sdk.internal.stream;

import com.shadowvc.sdk.internal.stream.connect.ConnectionLifeCycleListener;
import com.shadowvc.sdk.internal.stream.message.EopCometMessageListener;

/**
 * @author zhenzi
 */
public interface EopCometStream {

  /**
   * 设置连接监听
   *
   * @param connectionLifeCycleListener 监听
   */
  void setConnectionListener(ConnectionLifeCycleListener connectionLifeCycleListener);

  /**
   * 设置消息监听
   *
   * @param cometMessageListener 监听
   */
  void setMessageListener(EopCometMessageListener cometMessageListener);

  /**
   * 开启
   */
  void start();

  /**
   * 停止
   */
  void stop();

  /**
   * 在运行期添加新的长连接
   *
   * @param newClient 客户端
   */
  void addNewStreamClient(EopCometStreamRequest newClient);
}
