package com.shadowvc.sdk.internal.stream.message;

/**
 * 处理message的工厂类的配置信息
 *
 * @author zhenzi
 */
public interface MessageHandlerConfiguration {

  int getMinThreads();

  int getMaxThreads();

  int getQueueSize();
}
