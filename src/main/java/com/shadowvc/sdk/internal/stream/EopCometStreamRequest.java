package com.shadowvc.sdk.internal.stream;

import com.shadowvc.sdk.internal.util.ApiUtils;
import com.shadowvc.sdk.internal.stream.connect.ConnectionLifeCycleListener;
import com.shadowvc.sdk.internal.stream.message.EopCometMessageListener;
import com.shadowvc.sdk.internal.util.StringUtil;

/**
 * 请求参数类
 * <p>
 * File: EopCometStreamRequest.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class EopCometStreamRequest {

  // 应用id
  private String appKey;

  // 密钥
  private String secret;

  // 实例用户
  private Long instanceUserId;

  // 访问令牌，如果访问令牌是空那么会接收到这个用户实例下的所有用户消息
  private String accessToken;

  // 连接名称
  private String connectionName;

  private ConnectionLifeCycleListener connectListener;
  private EopCometMessageListener msgListener;

  public EopCometStreamRequest(String appKey, String secret, long instanceUserId, String accessToken,
                               String connectionName) {
    if (StringUtil.isEmpty(appKey)) {
      throw new RuntimeException("appkey is null");
    }
    if (StringUtil.isEmpty(secret)) {
      throw new RuntimeException("secret is null");
    }
    if (instanceUserId < 1) {
      throw new RuntimeException("instanceUserId is not validate");
    }
    if (StringUtil.isEmpty(connectionName)) {
      this.connectionName = ApiUtils.getLocalNetWorkIp();// 连接名称如果是空的就用ip地址，真是个好点子
    } else {
      this.connectionName = connectionName;
    }
    this.appKey = appKey;
    this.secret = secret;
    this.instanceUserId = instanceUserId;
    this.accessToken = accessToken;
  }

  public String getAppKey() {
    return appKey;
  }

  public String getSecret() {
    return secret;
  }

  public long getInstanceUserId() {
    return instanceUserId;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public ConnectionLifeCycleListener getConnectListener() {
    return connectListener;
  }

  public void setConnectListener(ConnectionLifeCycleListener connectListener) {
    this.connectListener = connectListener;
  }

  public EopCometMessageListener getMsgListener() {
    return msgListener;
  }

  public void setMsgListener(EopCometMessageListener msgListener) {
    this.msgListener = msgListener;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appKey == null) ? 0 : appKey.hashCode());
    result = prime * result + ((connectionName == null) ? 0 : connectionName.hashCode());
    result = prime * result + ((instanceUserId == null) ? 0 : instanceUserId.hashCode());
    result = prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EopCometStreamRequest other = (EopCometStreamRequest) obj;
    if (appKey == null) {
      if (other.appKey != null)
        return false;
    } else if (!appKey.equals(other.appKey))
      return false;
    if (connectionName == null) {
      if (other.connectionName != null)
        return false;
    } else if (!connectionName.equals(other.connectionName))
      return false;
    if (instanceUserId == null) {
      if (other.instanceUserId != null)
        return false;
    } else if (!instanceUserId.equals(other.instanceUserId))
      return false;
    if (accessToken == null) {
      if (other.accessToken != null)
        return false;
    } else if (!accessToken.equals(other.accessToken))
      return false;
    return true;
  }

}
