package com.shadowvc.sdk.internal.stream;

/**
 * 出现的系统级的异常，这类型的异常产生后需要整个系统退出 比如：missing app key,签名不对等
 * <p>
 * File: EopCometSysErrorException.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class EopCometSysErrorException extends EopCometException {

  private static final long serialVersionUID = 2890964819607554013L;

  public EopCometSysErrorException() {
    super();
  }

  public EopCometSysErrorException(String message, Throwable cause) {
    super(message, cause);
  }

  public EopCometSysErrorException(String message) {
    super(message);
  }

  public EopCometSysErrorException(Throwable cause) {
    super(cause);
  }

}
