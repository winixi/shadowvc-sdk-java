package com.shadowvc.sdk.internal.parser;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.internal.convert.FromJsonConverter;
import com.shadowvc.sdk.internal.util.ApiParser;
import com.shadowvc.sdk.internal.util.ApiResponse;

/**
 * 单个JSON对象解释器。
 *
 * @author carver.gu
 * @since 1.0, Apr 11, 2010
 */
public class JsonParser<T extends ApiResponse> implements ApiParser<T> {

  private Class<T> clazz;

  public JsonParser(Class<T> clazz) {
    this.clazz = clazz;
  }

  public T parse(String rsp) throws ApiException {
    return new FromJsonConverter().toResponse(rsp, clazz);
  }

  public Class<T> getResponseClass() {
    return clazz;
  }

}
