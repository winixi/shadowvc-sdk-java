package com.shadowvc.sdk.internal.convert;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.internal.json.ExceptionErrorListener;
import com.shadowvc.sdk.internal.json.JSONReader;
import com.shadowvc.sdk.internal.json.JSONValidatingReader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * JSON格式转换器。
 *
 * @author carver.gu
 * @since 1.0, Apr 11, 2010
 */
public class FromJsonConverter {

  /**
   * 生成返回对象
   *
   * @param rsp   返回内容
   * @param clazz 类型
   * @param <T>   eop对象
   * @return 返回
   * @throws ApiException 异常
   */
  public <T extends ApiResponse> T toResponse(String rsp, Class<T> clazz) throws ApiException {
    JSONReader reader = new JSONValidatingReader(new ExceptionErrorListener());
    Object rootObj = reader.read(rsp);
    if (rootObj instanceof Map<?, ?>) {
      Map<?, ?> rootJson = (Map<?, ?>) rootObj;
      Collection<?> values = rootJson.values();
      for (Object rspObj : values) {
        if (rspObj instanceof Map<?, ?>) {
          Map<?, ?> rspJson = (Map<?, ?>) rspObj;
          return fromJson(rspJson, clazz);
        }
      }
    }
    return null;
  }

  /**
   * 把JSON格式的数据转换为对象。
   *
   * @param <T>   泛型领域对象
   * @param json  JSON格式的数据
   * @param clazz 泛型领域类型
   * @return 领域对象
   * @throws ApiException
   */
  private <T> T fromJson(final Map<?, ?> json, Class<T> clazz) throws ApiException {
    return Converters.convert(clazz, new Reader() {
      public boolean hasReturnField(Object name) {
        return json.containsKey(name);
      }

      public Object getPrimitiveObject(Object name) {
        return json.get(name);
      }

      public Object getObject(Object name, Class<?> type) throws ApiException {
        Object tmp = json.get(name);
        if (tmp instanceof Map<?, ?>) {
          Map<?, ?> map = (Map<?, ?>) tmp;
          return fromJson(map, type);
        } else {
          return null;
        }
      }

      public List<?> getListObjects(Object listName, Object itemName, Class<?> subType) throws ApiException {
        List<Object> listObjs = null;

        Object listTmp = json.get(listName);

        if (listTmp instanceof List<?>) {
          listObjs = new ArrayList<>();
          List<?> tmpList = (List<?>) listTmp;
          for (Object subTmp : tmpList) {
            if (subTmp instanceof Map<?, ?>) {// object
              Map<?, ?> subMap = (Map<?, ?>) subTmp;
              Object subObj = fromJson(subMap, subType);
              if (subObj != null) {
                listObjs.add(subObj);
              }
            } else if (subTmp instanceof List<?>) {// array
              // TODO not support yet
            } else {// boolean, long, double, string, null
              listObjs.add(subTmp);
            }
          }
        }

        return listObjs;
      }
    });
  }

}
