package com.shadowvc.sdk.internal.convert;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.internal.util.ApiResponse;
import com.shadowvc.sdk.Constants;
import com.shadowvc.sdk.internal.annotation.ApiField;
import com.shadowvc.sdk.internal.annotation.ApiListField;
import com.shadowvc.sdk.internal.util.ObjectUtil;
import com.shadowvc.sdk.internal.util.StringUtil;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 转换工具类。
 *
 * @author carver.gu
 * @since 1.0, Apr 11, 2010
 */
public class Converters {

  public static boolean isCheckJsonType = false; // 是否对JSON返回的数据类型进行校验，默认不校验。给内部测试JSON返回时用的开关。规则：返回的"基本"类型只有String,Long,Boolean,Date,采取严格校验方式，如果类型不匹配，报错

  private static final Set<String> baseFields = new HashSet<>();

  static {
    baseFields.add("errorCode");
    baseFields.add("msg");
    baseFields.add("subCode");
    baseFields.add("subMsg");
    baseFields.add("responseBody");
    baseFields.add("requestParams");
    baseFields.add("success");
    baseFields.add("requestUrl");
  }

  private Converters() {
  }

  /**
   * 使用指定 的读取器去转换字符串为对象。
   *
   * @param <T>    领域泛型
   * @param clazz  领域类型
   * @param reader 读取器
   * @return 领域对象
   * @throws ApiException 异常
   */
  public static <T> T convert(Class<T> clazz, Reader reader) throws ApiException {
    T rsp;

    try {
      rsp = clazz.newInstance();
      BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
      PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();

      for (PropertyDescriptor pd : pds) {
        Method method = pd.getWriteMethod();
        if (method == null) { // ignore read-only fields
          continue;
        }

        String itemName = pd.getName();
        String listName = null;

        Field field;
        if (baseFields.contains(itemName) && ApiResponse.class.isAssignableFrom(clazz)) {
          field = ApiResponse.class.getDeclaredField(itemName);
        } else {
          field = clazz.getDeclaredField(itemName);
        }

        ApiField jsonField = field.getAnnotation(ApiField.class);
        ApiListField jsonListField = field.getAnnotation(ApiListField.class);
        if (jsonField != null) {
          itemName = jsonField.value();
          if (StringUtil.isEmpty(itemName)) {
            itemName = pd.getName();
          }
        }

        if (jsonListField != null) {
          listName = jsonListField.value();
          if (StringUtil.isEmpty(listName)) {
            listName = pd.getName();
          }
        }

        if (!reader.hasReturnField(itemName)) {
          if (listName == null || !reader.hasReturnField(listName)) {
            continue; // ignore non-return field
          }
        }

        Class<?> typeClass = field.getType();
        // 目前
        if (String.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof String) {
            method.invoke(rsp, value.toString());
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a String");
            }
            if (value != null) {
              method.invoke(rsp, value.toString());
            } else {
              method.invoke(rsp, "");
            }
          }
        } else if (Long.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof Long) {
            method.invoke(rsp, (Long) value);
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a Number(Long)");
            }
            if (StringUtil.isNumeric(value)) {
              method.invoke(rsp, Long.valueOf(value.toString()));
            }
          }
        } else if (Integer.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof Integer) {
            method.invoke(rsp, (Integer) value);
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a Number(Integer)");
            }
            if (StringUtil.isNumeric(value)) {
              method.invoke(rsp, Integer.valueOf(value.toString()));
            }
          }
        } else if (Boolean.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof Boolean) {
            method.invoke(rsp, (Boolean) value);
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a Boolean");
            }
            if (value != null) {
              method.invoke(rsp, Boolean.valueOf(value.toString()));
            }
          }
        } else if (Double.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof Double) {
            method.invoke(rsp, (Double) value);
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a Double");
            }
          }
        } else if (Number.class.isAssignableFrom(typeClass)) {
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof Number) {
            method.invoke(rsp, (Number) value);
          } else {
            if (isCheckJsonType && value != null) {
              throw new ApiException(itemName + " is not a Number");
            }
          }
        } else if (Date.class.isAssignableFrom(typeClass)) {
          DateFormat format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
          format.setTimeZone(TimeZone.getTimeZone(Constants.DATE_TIMEZONE));
          Object value = reader.getPrimitiveObject(itemName);
          if (value instanceof String) {
            method.invoke(rsp, format.parse(value.toString()));
          }
        } else if (typeClass.isEnum()) {
          String v = reader.getPrimitiveObject(itemName).toString().toUpperCase();
          if ("NULL".equals(v)) return null;
          Object value = Enum.valueOf((Class<Enum>) typeClass, v);
          method.invoke(rsp, value);
        } else if (List.class.isAssignableFrom(typeClass)) {
          Class<?> subType = ObjectUtil.getParameterizedType(field, 0);
          List<?> list = reader.getListObjects(listName, itemName, subType);
          if (list != null) {
            method.invoke(rsp, list);
          }
        } else {
          Object obj = reader.getObject(itemName, typeClass);
          if (obj != null) {
            method.invoke(rsp, obj);
          }
        }
      }
    } catch (Exception e) {
      throw new ApiException(e);
    }

    return rsp;
  }


}
