package com.shadowvc.sdk.internal.util;

import com.shadowvc.sdk.internal.annotation.ApiField;

import java.io.Serializable;
import java.util.Map;

/**
 * EOP请求返回
 * <p>
 * File: ApiResponse.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public abstract class ApiResponse implements Serializable {

  private static final long serialVersionUID = 5014379068811962022L;

  @ApiField
  private Integer errorCode;

  @ApiField
  private String msg;

  @ApiField
  private String subCode;

  @ApiField
  private String subMsg;

  private String requestUrl;
  private String responseBody;
  private Map<String, String> requestParams;

  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getSubCode() {
    return this.subCode;
  }

  public void setSubCode(String subCode) {
    this.subCode = subCode;
  }

  public String getSubMsg() {
    return this.subMsg;
  }

  public void setSubMsg(String subMsg) {
    this.subMsg = subMsg;
  }

  public String getResponseBody() {
    return responseBody;
  }

  public void setResponseBody(String responseBody) {
    this.responseBody = responseBody;
  }

  public Map<String, String> getRequestParams() {
    return requestParams;
  }

  public void setRequestParams(Map<String, String> requestParams) {
    this.requestParams = requestParams;
  }

  public boolean isSuccess() {
    return this.errorCode == null && this.subCode == null;
  }

  public String getRequestUrl() {
    return requestUrl;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

}
