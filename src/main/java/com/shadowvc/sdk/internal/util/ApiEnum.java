package com.shadowvc.sdk.internal.util;

/**
 * Created by chenxiaochun on 16/4/10.
 */
public interface ApiEnum {

  String getName();

  void setName(String name);
}
