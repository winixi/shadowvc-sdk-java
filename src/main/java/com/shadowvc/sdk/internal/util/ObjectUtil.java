package com.shadowvc.sdk.internal.util;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by chenxiaochun on 16/5/2.
 */
public class ObjectUtil {

  /**
   * 得到list的元素对象类型
   *
   * @param field 字段
   * @param index 索引
   * @return 返回
   */
  public static Class<?> getParameterizedType(Field field, int index) {
    Type fieldType = field.getGenericType();
    if (fieldType instanceof ParameterizedType) {
      ParameterizedType paramType = (ParameterizedType) fieldType;
      Type[] genericTypes = paramType.getActualTypeArguments();
      if (genericTypes != null && genericTypes.length > 0) {
        if (genericTypes[index] instanceof Class<?>) {
          Class<?> subType = (Class<?>) genericTypes[index];
          return subType;
        }
      }
    }
    return null;
  }

}
