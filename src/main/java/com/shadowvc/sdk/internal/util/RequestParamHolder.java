package com.shadowvc.sdk.internal.util;

/**
 * 请求参数容器
 * <p>
 * File: RequestParamHolder.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class RequestParamHolder {

  // 协议必填参数
  private ApiHashMap proMustParams;

  // 协议可选参数
  private ApiHashMap proOptParams;

  // 应用参数
  private ApiHashMap appParams;

  public ApiHashMap getProMustParams() {
    return proMustParams;
  }

  public void setProMustParams(ApiHashMap proMustParams) {
    this.proMustParams = proMustParams;
  }

  public ApiHashMap getProOptParams() {
    return proOptParams;
  }

  public void setProOptParams(ApiHashMap proOptParams) {
    this.proOptParams = proOptParams;
  }

  public ApiHashMap getAppParams() {
    return appParams;
  }

  public void setAppParams(ApiHashMap appParams) {
    this.appParams = appParams;
  }

}
