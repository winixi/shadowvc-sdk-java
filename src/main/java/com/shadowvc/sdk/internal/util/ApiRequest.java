package com.shadowvc.sdk.internal.util;

import com.shadowvc.sdk.exception.ApiRuleException;

/**
 * TOP请求接口。
 * 
 * @author carver.gu
 * @since 1.0, Sep 12, 2009
 */
public abstract class ApiRequest<T extends ApiResponse> {

  private Long timestamp;

  /**
   * 获取TOP的API名称。
   * 
   * @return API名称
   */
  public abstract String getApiMethodName();

  /**
   * 获取所有的Key-Value形式的文本请求参数集合。其中：
   * <ul>
   * <li>Key: 请求参数名</li>
   * <li>Value: 请求参数值</li>
   * </ul>
   * 
   * @return 文本请求参数集合
   */
  public abstract ApiHashMap getTextParams();

  /**
   * @return 指定或默认的时间戳
   */
  public Long getTimestamp() {
    return this.timestamp;
  }

  /**
   * 设置时间戳，如果不设置,发送请求时将使用当时的时间。
   * 
   * @param timestamp
   *          时间戳
   */
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * 获取返回对象类型
   * 
   * @return 返回
   */
  public abstract Class<T> getResponseClass();

  /**
   * 检查
   * 
   * @throws ApiRuleException 异常
   */
  public abstract void check() throws ApiRuleException;
}
