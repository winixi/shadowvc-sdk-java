package com.shadowvc.sdk.internal.util;

import java.io.Serializable;

/**
 * 基础类
 * 
 * File: ApiObject.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public abstract class ApiObject implements Serializable {

	private static final long serialVersionUID = 1L;

}
