package com.shadowvc.sdk.internal.util;

import java.util.Map;

/**
 * EOP上传请求接口，支持同时上传多个文件。
 * 
 * File: ApiUploadRequest.java
 * Description:
 * 
 * Copyright: Copyright (c) 2014 ecbox.com
 * Company: ECBOX,Inc.
 * 
 * @author chenxiaochun
 * @version 1.0
 */
public abstract class ApiUploadRequest<T extends ApiResponse> extends ApiRequest<T> {

	/**
	 * 获取所有的Key-Value形式的文件请求参数集合。其中：
	 * <ul>
	 * <li>Key: 请求参数名</li>
	 * <li>Value: 请求参数文件元数据</li>
	 * </ul>
	 * 
	 * @return 文件请求参数集合
	 */
	public abstract Map<String, FileItem> getFileParams();

}
