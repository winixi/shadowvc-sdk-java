package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.*;

/**
 * 包装字段
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
@Documented
public @interface ApiWrapperField {

  /**
   * 数据包装字段
   *
   * @return
   */
  String value() default "";

}
