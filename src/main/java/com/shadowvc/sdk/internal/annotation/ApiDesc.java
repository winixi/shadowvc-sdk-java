package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.*;

/**
 * api字段描述
 * <p>
 * File: ApiDesc.java<br/>
 * Description: <br/>
 * <p>
 * CopyrightMapper: CopyrightMapper (c) 2012 ecbox.com<br/>
 * Company: ECBOX,Inc.<br/>
 *
 * @author chenxiaochun
 * @version 1.0
 * @date 2013-4-21
 */
@Retention(RetentionPolicy.CLASS)
@Target(value = {ElementType.FIELD})
@Documented
public @interface ApiDesc {

  /**
   * 文档内容
   *
   * @return
   */
  String value() default "";
}
