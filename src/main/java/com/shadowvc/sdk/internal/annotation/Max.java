package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数值最大值
 * 
 * File: Max.java<br/>
 * Description: <br/>
 *
 * CopyrightMapper: CopyrightMapper (c) 2012 ecbox.com<br/>
 * Company: ECBOX,Inc.<br/>
 *
 * @author chenxiaochun
 * @date 2013-4-21
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
@Documented
public @interface Max {

	/**
	 * 错误消息
	 * 
	 * @return
	 */
	String message() default "超出最大值";
	
	/**
	 * 最大值
	 * 
	 * @return
	 */
	long value();
}
