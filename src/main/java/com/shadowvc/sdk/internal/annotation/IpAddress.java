package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.*;

/**
 * ip地址
 * 
 * File: IpAddress.java<br/>
 * Description: <br/>
 *
 * CopyrightMapper: CopyrightMapper (c) 2012 ecbox.com<br/>
 * Company: ECBOX,Inc.<br/>
 *
 * @author chenxiaochun
 * @date 2013-4-22
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
@Documented
public @interface IpAddress {

	/**
	 * 返回错误信息
	 * 
	 * @return
	 */
	String message() default "ip地址格式不正确";
}
