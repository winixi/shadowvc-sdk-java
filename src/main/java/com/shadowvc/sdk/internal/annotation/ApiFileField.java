package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.*;

/**
 * 文件字段
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
@Documented
public @interface ApiFileField {

  /**
   * JSON列表属性映射名称
   *
   * @return
   */
  String value() default "";

}
