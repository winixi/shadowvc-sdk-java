package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 是url地址
 * 
 * File: Url.java<br/>
 * Description: <br/>
 * 
 * CopyrightMapper: CopyrightMapper (c) 2012 ecbox.com<br/>
 * Company: ECBOX,Inc.<br/>
 * 
 * @author chenxiaochun
 * @date 2013-4-22
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
@Documented
public @interface Url {

	/**
	 * 返回错误信息
	 * 
	 * @return
	 */
	String message() default "url地址不正确";
}
