package com.shadowvc.sdk.internal.annotation;

import java.lang.annotation.*;

/**
 * 对象字段
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
@Documented
public @interface ApiObjectField {

  /**
   * JSON列表属性映射名称
   *
   * @return
   */
  String value() default "";

}
