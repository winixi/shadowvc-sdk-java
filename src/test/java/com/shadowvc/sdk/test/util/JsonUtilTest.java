package com.shadowvc.sdk.test.util;

import com.shadowvc.sdk.internal.json.JSONWriter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chenxiaochun on 16/4/7.
 */
public class JsonUtilTest {

  @Test
  public void test() {
    Map<String, Object> data = initData();
    System.out.println(new JSONWriter().write(data));
  }

  @Test
  public void test2() {
    List<String> list = new ArrayList<>();
    list.add("ads");
    list.add("111");
    System.out.println(new JSONWriter().write(list));
  }

  private static Map<String, Object> initData() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("eventName", "ApplicationDetailUpdateAfter");
    map.put("source", "11111-22222-33333");
    map.put("age", 123.333);

    Map<String, String> paramMap = new HashMap<String, String>();
    map.put("parameters", paramMap);

    paramMap.put("recordId", "REC14-00000-00013");
    paramMap.put("recordType", "ggg-aaa-cc-dd");

    List<String> countryList = new ArrayList<String>();
    map.put("country", countryList);
    countryList.add("China");
    countryList.add("USA");

    List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
    map.put("mapList", mapList);
    Map<String, Object> tempMap = new HashMap<String, Object>();
    tempMap.put("name", "zzz");
    tempMap.put("nick", "ttt");
    mapList.add(tempMap);
    return map;
  }
}
