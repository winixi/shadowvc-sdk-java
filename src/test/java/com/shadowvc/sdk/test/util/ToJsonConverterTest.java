package com.shadowvc.sdk.test.util;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.internal.util.ApiHashMap;
import com.shadowvc.sdk.domain.Category;
import com.shadowvc.sdk.internal.convert.ToJsonConverter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenxiaochun on 16/4/8.
 */
public class ToJsonConverterTest {

  @Test
  public void test1() throws ApiException {
    Category obj = new Category();
    obj.setName("我的人");
    obj.setId(1);
    System.out.println(new ToJsonConverter().toString(obj));
  }

  @Test
  public void test2() throws ApiException {
    List<String> list = new ArrayList<>();
    list.add("ads");
    list.add("111");
    //Class<?> subType = ObjectUtil.getParameterizedType(field, 0);//得到列表元素类型
    System.out.println(new ToJsonConverter().toString(list, String.class));
  }

  @Test
  public void test3() throws ApiException {
    List<Category> list = new ArrayList<>();
    Category obj = new Category();
    obj.setName("我的人");
    obj.setId(1);
    list.add(obj);
    list.add(obj);
    List<Integer> tags = new ArrayList<>();
    tags.add(1);
    tags.add(2);
    System.out.println(new ToJsonConverter().toString(list, Category.class));
  }

  @Test
  public void test4() {
    ApiHashMap map = new ApiHashMap();

    //obj
    Category obj = new Category();
    obj.setName("我的人");
    obj.setId(1);
    map.put("obj", obj);

    //list
    List<Category> tags = new ArrayList<>();
    tags.add(obj);
    tags.add(obj);
    map.put("categories", tags);

    System.out.println("adfsdf");

  }
}
