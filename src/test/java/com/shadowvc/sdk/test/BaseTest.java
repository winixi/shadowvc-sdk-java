package com.shadowvc.sdk.test;

import com.shadowvc.sdk.ApiClient;
import com.shadowvc.sdk.DefaultApiClient;

/**
 * 基本代码
 * <p>
 * File: BaseTest.java
 * Description:
 * <p>
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class BaseTest {

//  public final static String SERVER_URL = "http://localhost:8082/rest";
  public final static String SERVER_URL = "http://demo.api.shadowvc.com/rest";
//  public final static String SERVER_URL = "http://api.shadowvc.com/rest";

  public final static String APP_KEY = "MODULE-20140629174446096";
  public final static String SECRET = "z3aUdadaU2cmAeAA";

  public ApiClient getClient() {
    ApiClient client = new DefaultApiClient(SERVER_URL, APP_KEY, SECRET);
    return client;
  }
}
