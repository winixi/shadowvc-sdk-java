package com.shadowvc.sdk.test.request.report;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ReportCreateRequest;
import com.shadowvc.sdk.request.ReportGetRequest;
import com.shadowvc.sdk.response.ReportCreateResponse;
import com.shadowvc.sdk.response.ReportGetResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by chenxiaochun on 16/5/28.
 */
public class ReportGetTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ReportGetRequest request = new ReportGetRequest();
    request.setReportId(4);

    ReportGetResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getReport());
  }
}
