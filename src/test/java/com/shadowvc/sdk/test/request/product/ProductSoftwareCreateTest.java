package com.shadowvc.sdk.test.request.product;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.domain.Copyright;
import com.shadowvc.sdk.request.ProductSoftwareCreateRequest;
import com.shadowvc.sdk.response.ProductSoftwareCreateResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ProductSoftwareCreateTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ProductSoftwareCreateRequest request = new ProductSoftwareCreateRequest();
    request.setCompanyId(1);
    request.setName("测试号产品");
    request.setCategoryId(1);
    Copyright c = new Copyright();
    c.setId(1);
    c.setName("号");
    c.setNumber("1");
    c.setVersion("2.0");
    request.setCopyright(c);
    ProductSoftwareCreateResponse resp = getClient().execute(request);

    if (!resp.isSuccess()) System.out.println(resp.getErrorCode());

    System.out.println();
    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
