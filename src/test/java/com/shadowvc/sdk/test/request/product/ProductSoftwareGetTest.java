package com.shadowvc.sdk.test.request.product;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ProductSoftwareGetRequest;
import com.shadowvc.sdk.response.ProductSoftwareGetResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ProductSoftwareGetTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ProductSoftwareGetRequest request = new ProductSoftwareGetRequest();
    request.setUuid("ff16d3ff11424a478f2896ca621b814c");
    ProductSoftwareGetResponse resp = getClient().execute(request);

    if (!resp.isSuccess()) System.out.println(resp.getErrorCode());

    System.out.println();
    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
