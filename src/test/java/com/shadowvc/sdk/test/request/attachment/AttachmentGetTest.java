package com.shadowvc.sdk.test.request.attachment;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.AttachmentGetRequest;
import com.shadowvc.sdk.response.AttachmentGetResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chenxiaochun on 16/5/26.
 */
public class AttachmentGetTest extends BaseTest {

  @Test
  public void test2() throws ApiException {
    AttachmentGetRequest request = new AttachmentGetRequest();
    request.setStorageKey("wzOUbO20");
    request.setPathVariable("");
    request.setExpires(30L);

    AttachmentGetResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getAttachment().getUrl());
  }
}
