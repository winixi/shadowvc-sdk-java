package com.shadowvc.sdk.test.request.attachment;

import com.shadowvc.sdk.internal.util.FileItem;
import com.shadowvc.sdk.request.AttachmentUploadRequest;
import com.shadowvc.sdk.response.AttachmentUploadResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chenxiaochun on 16/5/26.
 */
public class AttachmentUploadTest extends BaseTest {

  @Test
  public void test() throws Exception {
    AttachmentUploadRequest request = new AttachmentUploadRequest();
    String path = this.getClass().getClassLoader().getResource("dat/t.jpg").getPath();
    FileItem file = new FileItem(path);

    request.setFileItem(file);


    AttachmentUploadResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getAttachment().getUrl());
  }
}
