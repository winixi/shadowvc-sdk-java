package com.shadowvc.sdk.test.request.report;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ReportCreateRequest;
import com.shadowvc.sdk.response.ReportCreateResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by chenxiaochun on 16/5/28.
 */
public class ReportCreateTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ReportCreateRequest request = new ReportCreateRequest();
    request.setWxOpenId("1234");
    request.setName("陈晓春");
    request.setEmail("winixi@ecbox.com");
    request.setLabel("使用麻烦");
    request.setContent("内容不健康");
    request.setMobile("1777777777");

    request.setStorageKeys(Arrays.asList("wzOUbO20"));


    ReportCreateResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getReportId());
  }
}
