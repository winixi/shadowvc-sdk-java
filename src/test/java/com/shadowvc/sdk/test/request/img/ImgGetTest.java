package com.shadowvc.sdk.test.request.img;

import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ImgGetRequest;
import com.shadowvc.sdk.response.ImgGetResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chenxiaochun on 16/5/26.
 */
public class ImgGetTest extends BaseTest {


  @Test
  public void test2() throws ApiException {
    ImgGetRequest request = new ImgGetRequest();
    request.setStorageKey("V6rExQwV");
    ImgGetResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getImg().getUrl());
  }
}
