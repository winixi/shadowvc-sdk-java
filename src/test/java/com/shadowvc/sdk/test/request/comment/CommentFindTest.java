package com.shadowvc.sdk.test.request.comment;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.request.CommentFindRequest;
import com.shadowvc.sdk.response.CommentFindResponse;
import org.junit.Test;

/**
 * 测试留言查询
 */
public class CommentFindTest extends BaseTest {

  @Test
  public void test() throws Exception {
    CommentFindRequest request = new CommentFindRequest();
    //request.setCompanyId(null);
    request.setOutId("e7ebb84521734a399f816e614e59c819");
    request.setLabel("PRODUCT");
    request.setWxOpenId("11081");
    CommentFindResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    System.out.println(resp);
  }
}
