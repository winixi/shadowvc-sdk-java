package com.shadowvc.sdk.test.request.product;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ProductSoftwareCountRequest;
import com.shadowvc.sdk.response.ProductSoftwareCountResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ProductSoftwareCountTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ProductSoftwareCountRequest request = new ProductSoftwareCountRequest();
    ProductSoftwareCountResponse resp = getClient().execute(request);

    if (!resp.isSuccess()) System.out.println(resp.getErrorCode());

    System.out.println();
    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
