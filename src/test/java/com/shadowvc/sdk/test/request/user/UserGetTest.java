package com.shadowvc.sdk.test.request.user;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.UserGetRequest;
import com.shadowvc.sdk.response.UserGetResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class UserGetTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    UserGetRequest request = new UserGetRequest();

    request.setUserId(1);
    UserGetResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);

  }
}
