package com.shadowvc.sdk.test.request.category;

import com.shadowvc.sdk.test.BaseTest;

import com.shadowvc.sdk.request.CategoryGetRequest;
import com.shadowvc.sdk.response.CategoryGetResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 公司信息获取测试
 * 
 * File: CompanyGetTest.java
 * Description:
 * 
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 * 
 * @author chenxiaochun
 * @version 1.0
 */
public class CategoryGetTest extends BaseTest {

  private int categoryId = 12;

  @Test
  public void test() throws Exception {
    CategoryGetRequest request = new CategoryGetRequest();
    request.setCategoryId(categoryId);
    CategoryGetResponse resp = getClient().execute(request);
    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
