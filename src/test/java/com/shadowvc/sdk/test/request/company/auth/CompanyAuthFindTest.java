package com.shadowvc.sdk.test.request.company.auth;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.dict.AuthStatus;
import com.shadowvc.sdk.request.CompanyAuthFindRequest;
import com.shadowvc.sdk.response.CompanyAuthFindResponse;
import org.junit.Test;

import java.util.Arrays;

/**
 *
 * File: CompanyAuthFindTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class CompanyAuthFindTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    CompanyAuthFindRequest request = new CompanyAuthFindRequest();
    request.setAuthStatuses(Arrays.asList(AuthStatus.UNPROCESSED));
    CompanyAuthFindResponse resp = getClient().execute(request);

    System.out.println(resp.getRequestParams());
    System.out.println(resp.getResponseBody());
    System.out.println(resp.getList().get(0).getAuthStatus());

  }
}
