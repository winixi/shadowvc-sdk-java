package com.shadowvc.sdk.test.request.problem;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.ProblemGetRequest;
import com.shadowvc.sdk.response.ProblemGetResponse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ProblemGetTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ProblemGetRequest request = new ProblemGetRequest();

    request.setUuid("c193724d376d46fbb0a13a1baf25158f");
    List<String> fields = new ArrayList<>();
    fields.add("tags");
    request.setFields(fields);
    ProblemGetResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    System.out.println(resp.getProblem().getTitle());


  }
}
