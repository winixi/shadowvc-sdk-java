package com.shadowvc.sdk.test.request.company;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.request.CompanyGetRequest;
import com.shadowvc.sdk.response.CompanyGetResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * 公司信息获取测试
 * 
 * File: CompanyGetTest.java
 * Description:
 * 
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 * 
 * @author chenxiaochun
 * @version 1.0
 */
public class CompanyGetTest extends BaseTest {

  @Test
  public void test() throws Exception {
    CompanyGetRequest request = new CompanyGetRequest();
    request.setCompanyId(2);
    CompanyGetResponse resp = getClient().execute(request);
    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
