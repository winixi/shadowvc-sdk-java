package com.shadowvc.sdk.test.request.version;

import com.shadowvc.sdk.dict.AppType;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.request.VersionCreateRequest;
import com.shadowvc.sdk.response.VersionCreateResponse;
import com.shadowvc.sdk.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * 用户信息请求测试
 *
 * File: UserGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class VersionCreateTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    VersionCreateRequest request = new VersionCreateRequest();

    request.setAppType(AppType.IOS);
    request.setForceUpdate(true);
    request.setMemo("测试");
    request.setUrl("http://www.shadowvc.com");
    request.setVer(1.11);

    VersionCreateResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);

  }
}
