package com.shadowvc.sdk.test.request.category;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.request.CategoryCreateRequest;
import com.shadowvc.sdk.response.CategoryCreateResponse;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 公司信息获取测试
 *
 * File: CompanyGetTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class CategoryCreateTest extends BaseTest {

  @Test
  public void test() throws Exception {
    CategoryCreateRequest request = new CategoryCreateRequest();
    request.setName("测试分类");
    request.setLabel("test");
    List<String> tags = new ArrayList<>();
    tags.add("分类1");
    tags.add("分类2");
    CategoryCreateResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
  }
}
