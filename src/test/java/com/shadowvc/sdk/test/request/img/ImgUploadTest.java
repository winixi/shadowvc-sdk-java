package com.shadowvc.sdk.test.request.img;

import com.shadowvc.sdk.test.BaseTest;
import com.shadowvc.sdk.exception.ApiException;
import com.shadowvc.sdk.dict.StorageType;
import com.shadowvc.sdk.internal.util.FileItem;
import com.shadowvc.sdk.request.ImgUploadRequest;
import com.shadowvc.sdk.response.ImgUploadResponse;
import org.junit.Assert;
import org.junit.Test;


/**
 * 图片上传测试
 *
 * File: ImgUploadTest.java
 * Description:
 *
 * Copyright: Copyright (c) 2012 ecbox.com
 * Company: ECBOX,Inc.
 *
 * @author chenxiaochun
 * @version 1.0
 */
public class ImgUploadTest extends BaseTest {

  @Test
  public void test() throws ApiException {
    ImgUploadRequest request = new ImgUploadRequest();
    String path = this.getClass().getClassLoader().getResource("dat/1.jpg").getPath();
    FileItem file = new FileItem(path);
    request.setFileItem(file);
    request.setStorageType(StorageType.PUBLIC);//公共


    ImgUploadResponse resp = getClient().execute(request);

    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getImg().getUrl());
  }

  @Test
  public void test2() throws ApiException {
    ImgUploadRequest request = new ImgUploadRequest();
    String path = this.getClass().getClassLoader().getResource("dat/w650.jpg").getPath();
    FileItem file = new FileItem(path);

    request.setFileItem(file);
    request.setStorageType(StorageType.PRIVATE);//私有
    ImgUploadResponse resp = getClient().execute(request);


    System.out.println(resp.getResponseBody());
    Assert.assertNotNull(resp);
    System.out.println(resp.getImg().getUrl());
  }

}
